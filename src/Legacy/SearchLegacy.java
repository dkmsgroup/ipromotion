/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Legacy;

import common.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom2.JDOMException;

/**
 *
 * @author Administrator
 */
public class SearchLegacy {
    
    String elementName;
    String attributeName;
    String attributeValue;
    
    File resultsFile;
    
    /*
     * Scan mpeg7 folder for mpeg7 files.
     * Scan x3d folder for x3d files.
     * Add mpeg7 and x3d files to a Files Array allFiles.
     * Return allFiles.
     */
    
    private File[] GetFiles(File mpeg7Folder, File x3dFolder){
        
        File[] mpeg7Files = null;
        File[] x3dFiles = null;
        File[] allFiles;
        
        if(mpeg7Folder.exists() && x3dFolder.exists()){
            mpeg7Files = mpeg7Folder.listFiles(new FilterXMLFiles());
            x3dFiles = x3dFolder.listFiles(new FilterX3DFiles());
        }

        allFiles = new File[mpeg7Files.length + x3dFiles.length];
        int index=0;
        
        for(int j=0; j<mpeg7Files.length; j++){
            allFiles[index] = mpeg7Files[j];
            index++;
        }
        
        for(int j=0; j<x3dFiles.length; j++){
            allFiles[index] = x3dFiles[j];
            index++;
        }
        
        return allFiles;
        
    }
    
    /*
     * Search files from File Array allFiles.
     * Results written to resultsFile.
     */
    
    private void SubmitQuery(File[] allFiles) throws IOException, JDOMException{
        
        boolean matchQuery;
        String filePath;
        File curFile;
        
        for(int i=0; i<allFiles.length; i++){
            
            filePath = allFiles[i].getPath();
            
            File xmlFile = new File(filePath);
            
            NavigateXML navigate = new NavigateXML(elementName, attributeName, attributeValue);
            matchQuery = navigate.QueryXMLFile(xmlFile);
            
            /*
             * If query returns true write result to results file.
             */
            
            if(matchQuery){
                curFile = new File(filePath);
                try (FileWriter fileWriter = new FileWriter(resultsFile, true)) {
                    fileWriter.write(new ProcessFilename().RemoveSuffix(curFile.getName()) + "\n");
                }
            }
        }
        
    }
    
    /*
     * Get Files and Submit Query.
     */
    
    public void Search(File mpeg7Folder, File x3dFolder) throws IOException, JDOMException{
        
        File[] allFiles = GetFiles(mpeg7Folder, x3dFolder);
        
        SubmitQuery(allFiles);
        
    }
    
    /*
     * Constructor
     */
    
    public SearchLegacy(String elName, String atName, String atValue, String path, String resultsFileName) throws IOException{
        elementName = elName;
        attributeName = atName;
        attributeValue = atValue;
        
        /*
         * Results written to path + "Legacy\\results\\" folder.
         */
        
        String resultsSubPath = "Legacy\\";
        File jsonSubFolder = new File(path + resultsSubPath);
        
        if(!jsonSubFolder.exists())
            jsonSubFolder.mkdir();
        
        resultsSubPath = "Legacy\\results\\";
        String resultsFolderPath = path + resultsSubPath;
        
        File resultsFolder = new File(resultsFolderPath);
        
        if(!resultsFolder.exists()){
            resultsFolder.mkdir();
        }
        
        /*
         * Compose name of results file.
         * Each query creates its own file.
         * If a file with the same name exists, overwrite it.
         */
        
        resultsFileName += ".txt";
        String resultsFilePath = resultsFolderPath + resultsFileName;
        
        resultsFile = new File(resultsFilePath);
        
        if(!resultsFile.exists()){
            resultsFile.createNewFile();
        }
        else{
            try (FileWriter fileWriter = new FileWriter(resultsFile)) {
                fileWriter.flush();
            }
        }
    }
}
