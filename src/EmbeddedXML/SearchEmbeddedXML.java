/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EmbeddedXML;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Bytes;
import common.NavigateXML;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom2.JDOMException;

/**
 *
 * @author Administrator
 */
public class SearchEmbeddedXML {
    
    String elementName;
    String attributeName;
    String attributeValue;
    
    File resultsFile;
    DBCollection collection;
    DB db;
    
    private void SubmitQuery(String xmlString, String name) throws JDOMException, IOException{
        
        boolean matchQuery;
        
        NavigateXML navigate = new NavigateXML(elementName, attributeName, attributeValue);
        matchQuery = navigate.QueryXMLSrting(xmlString);
        
        /*
         * If query returns true write result to results file.
         */
        
        if(matchQuery){
            try (FileWriter fileWriter = new FileWriter(resultsFile, true)) {
                    fileWriter.write(name + "\n");
                }
        }
    }
    
    /*
     * Get all documents of MongoDB collection "PathXML".
     * Get value of keys "Mpeg7" and "X3D" (the XML Description as String).
     * Submit Query to value.
     */
    
    public void Search() throws IOException, JDOMException{
        
        String xmlString;
        
        /*
         * QUERYOPTION_NOTIMEOUT is used so that the cursor doesn't timeout.
         * Happens in large databases.
         * Which one should be used?
         */
        
        DBCursor cursor = collection.find().addOption(new Bytes().QUERYOPTION_NOTIMEOUT);
        //DBCursor cursor = collection.find().addOption(Bytes.QUERYOPTION_NOTIMEOUT);
        
        while (cursor.hasNext()) {
            
            BasicDBObject nextDoc = (BasicDBObject) cursor.next();
            
            if(nextDoc.containsField("Mpeg7")){
                xmlString = (String) nextDoc.get("Mpeg7");
                SubmitQuery(xmlString, nextDoc.getString("name"));
            }
            
            if(nextDoc.containsField("X3D")){
                xmlString = (String) nextDoc.get("X3D");
                SubmitQuery(xmlString, nextDoc.getString("name"));
            }
            
            //System.out.println("Done with " + nextDoc.getString("name"));
            
	}
        
    }
    
    /*
     * Constructor
     */
    
    public SearchEmbeddedXML(String elName, String atName, String atValue, String path, String resultsFileName, DB database) throws IOException{
        elementName = elName;
        attributeName = atName;
        attributeValue = atValue;
        
        /*
         * Results written to path + "EmbeddedXML\\results\\" folder.
         */
        
        String resultsSubPath = "EmbeddedXML\\";
        File jsonSubFolder = new File(path + resultsSubPath);
        
        if(!jsonSubFolder.exists())
            jsonSubFolder.mkdir();
        
        resultsSubPath = "EmbeddedXML\\results\\";
        String resultsFolderPath = path + resultsSubPath;
        
        File resultsFolder = new File(resultsFolderPath);
        
        if(!resultsFolder.exists()){
            resultsFolder.mkdir();
        }
        
        /*
         * Compose name of results file.
         * Each query creates its own file.
         * If a file with the same name exists, overwrite it.
         */
        
        resultsFileName += ".txt";
        String resultsFilePath = resultsFolderPath + resultsFileName;
        
        resultsFile = new File(resultsFilePath);
        
        if(!resultsFile.exists()){
            resultsFile.createNewFile();
        }
        else{
            try (FileWriter fileWriter = new FileWriter(resultsFile)) {
                fileWriter.flush();
            }
        }
        
        String colName = "EmbeddedXML";
        db = database;
        collection = db.getCollection(colName);
    
    }
}
