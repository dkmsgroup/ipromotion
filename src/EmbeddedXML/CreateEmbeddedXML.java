/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EmbeddedXML;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import common.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import org.json.JSONException;

/**
 *
 * @author Lydia
 */

public class CreateEmbeddedXML {
    
    String jsonFolderPath;
    DBCollection collection;
    
    /*
     * Take all files from list and add content in key/value pairs with
     * key = "Mpeg7" or "X3D" and value the description of Mpeg7 and X3D as a string.
     */
    
    private void AddFileContentTOJSONFile(File[] listOfFiles) throws IOException, FileNotFoundException, JSONException {
        
        String fileName, rawFileName, jsonFileName, jsonFilePath;
        String key, value;
        boolean jsonFileExists;
        
        for(int i=0; i<listOfFiles.length; i++){
            
            fileName = listOfFiles[i].getName();
            rawFileName = new ProcessFilename().RemoveSuffix(fileName);
            jsonFileName = rawFileName + ".json";
            jsonFilePath = jsonFolderPath + jsonFileName;
            
            /*
             * Check if JSON file exists within the JSON folder.
             */
            
            jsonFileExists = new File(jsonFolderPath,jsonFileName).exists();
            
            /*
             * If JSON file doesn't exist, create a key/value pair,
             * with key = "name" and value = "filename".
             * Call CreateJSONFile.
             */
            
            if(!jsonFileExists){
                key = "name";
                value = rawFileName;
                
                new ModifyJSONFile().CreateJSONFile(key,value,jsonFilePath);
            }
            
            Scanner xmlContent = new Scanner(listOfFiles[i]);
            String xmlString = xmlContent.useDelimiter("\\Z").next();
            
            /*
             * Update the JSON file with a key/value pair 
             * where key is the description name (Mpeg7 or X3D)
             * and value the description as a String.
             */
            
            key = new ProcessFilename().GetSuffix(fileName);
            if(key.equalsIgnoreCase(".xml"))
                key = "Mpeg7";
            else if(key.equalsIgnoreCase(".x3d"))
                key = "X3D";
            
            value = xmlString;
            
            new ModifyJSONFile().UpdateJSONFile(key,value,jsonFilePath);
        }
    }
    
    /*
     * Take an xml folder and an x3d folder.
     * Add key/value pairs to JSON files in a json folder.
     * If JSON file doesn't exists create a unique "name" key/value pair, where key = "name" and value = "filename".
     */
    
    public void CreateAllJSONFiles (File mpeg7Folder, File x3dFolder) throws IOException, FileNotFoundException, JSONException{
        
        File jsonFolder = new File(jsonFolderPath);
        
        if(jsonFolder.exists())
            new DeleteFolder().DeleteFolder(jsonFolder);
        
        jsonFolder.mkdir();
        
        if (mpeg7Folder.exists() && x3dFolder.exists()){
            
            /*
             * Create list of files in xml and x3d directories that have .xml and .x3d extentions.
             */
            
            File[] listOfxmlFiles = mpeg7Folder.listFiles(new FilterXMLFiles());
            File[] listOfx3dFiles = x3dFolder.listFiles(new FilterX3DFiles());
                        
            AddFileContentTOJSONFile(listOfxmlFiles);
            AddFileContentTOJSONFile(listOfx3dFiles);
                        
        }
        
        /*
         * Delete previous collection.
         * Add files from jsonFolderPath to collection.
         */
        
        collection.drop();
        new AddToMongoDB().FolderToMongoDB(jsonFolderPath, collection);
    }
    
    /*
     * Constructor.
     */
    
    public CreateEmbeddedXML(String jFolderPath, DB db){
        
        String jsonSubPath = "EmbeddedXML\\";
        String jsonWholePath = "EmbeddedXML\\json\\";
        
        File jsonSubFolder = new File(jFolderPath + jsonSubPath);
        
        if(!jsonSubFolder.exists())
            jsonSubFolder.mkdir();
        
        jsonFolderPath = jFolderPath + jsonWholePath;
        
        String colName = "EmbeddedXML";
        collection = db.getCollection(colName);
    }
}
