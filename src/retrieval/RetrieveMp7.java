/*******************************************************************************
 * Copyright (C) 2014 Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package retrieval;

//Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[0].BoundingBox3DSize.BoxHeight

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jaxen.function.SumFunction;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Giorgos
 */
public class RetrieveMp7 {

	private static final int NUM_CELLS = 512;
	
    public static void main(String[] args) throws JSONException{
    	try {
            Mongo mongo = new Mongo("localhost", 27017);
            DB Mydb = mongo.getDB("X3D");
            
            BasicDBObject info = new BasicDBObject();
            
            DBCollection collection = Mydb.getCollection("Savage.files");
                     
            BasicDBObject qu = new BasicDBObject();
            qu.append("filename", "Bear-Russia1");
            DBCursor cursor = collection.find();
            JSONObject obj = new JSONObject();
            Gson gson = new GsonBuilder().create();
            double length, height, depth, width, volume;
            int cell;
            double[] histogram = new double[NUM_CELLS];
            double[] sceneHistogram = new double[NUM_CELLS];
            int[] numOccurences = new int[NUM_CELLS];
            double sceneVolumeSum = 0;
            
            while(cursor.hasNext()){//return the file specified by "filename"
            	info = (BasicDBObject) cursor.next().get("metadata");		//get the file metadata - mp7 description
            	obj = new JSONObject(info.toString());
          //  	System.out.println(obj.toString());               
        		sceneHistogram = new double[NUM_CELLS];
        		for(int i = 0; i < sceneHistogram.length; i++){
        			sceneHistogram[i] = 0.0;
        		}
        		sceneVolumeSum = 0.0;
            JsonParser jsonParser = new JsonParser( );
            try{
            JsonArray DescriptorCollection = jsonParser.parse(obj.toString())
                .getAsJsonObject().get("Mpeg7")
                .getAsJsonObject().getAsJsonArray("Description").get(1)
                .getAsJsonObject().get("MultimediaContent")
                .getAsJsonObject().get("StructuredCollection")
                .getAsJsonObject().getAsJsonArray("Collection").get(1)
                .getAsJsonObject().getAsJsonArray("DescriptorCollection");		//j
                
	            length = DescriptorCollection.size();
	            for(int i = 0; i < length; i++){
	            	try{
		            	JsonElement BoundingBox3DSize = DescriptorCollection.getAsJsonArray().get(i).getAsJsonObject()
		            							  .getAsJsonObject().getAsJsonArray("Descriptor").get(0)
		            							  .getAsJsonObject().get("BoundingBox3DSize");
			
		            	height = BoundingBox3DSize.getAsJsonObject().get("BoxHeight").getAsDouble();
		            	width = BoundingBox3DSize.getAsJsonObject().get("BoxWidth").getAsDouble();
		            	depth = BoundingBox3DSize.getAsJsonObject().get("BoxDepth").getAsDouble();
		            	volume = height * width * depth;
	            		//System.out.println("volume: " + volume);

	            		//this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[1].Geometry3D.DominantColor3D.Value.Index;"
	            		JsonElement RGB = DescriptorCollection.getAsJsonArray().get(i).getAsJsonObject()
  							  .getAsJsonObject().getAsJsonArray("Descriptor").get(1)
  							  .getAsJsonObject().get("Geometry3D")
  							  .getAsJsonObject().get("DominantColor3D")
  							  .getAsJsonObject().get("Value")
  							  .getAsJsonObject().get("Index");
	            			            		
	            		//Since we reached here, we have gotten both the volume and the color
	            		//Otherwise an exception would have been thrown
	            		String[] RGBarr = RGB.getAsString().split(" ");
	            		System.out.println(RGB.getAsString());
	            		cell = (int) (Math.floor((double) Integer.parseInt(RGBarr[0]) / 32) + 8 * Math.floor((double) Integer.parseInt(RGBarr[1]) / 32) + 64 * Math.floor((double) Integer.parseInt(RGBarr[2]) / 32) );
	            		System.out.println(cell);
	            		sceneHistogram[cell] += volume;
	            		numOccurences[cell] ++;
	            		sceneVolumeSum += volume;
	            	}
	            	catch(Exception e1){
	        //    		e1.printStackTrace();	            		
	            	}
	            }
            	if(sceneVolumeSum == 0.0){
            		System.out.println("Something went wrong");
            		continue;
            	}
	            for(int i = 0; i < NUM_CELLS; i++){
	            	sceneHistogram[i] /= sceneVolumeSum;
	            	histogram[i] += sceneHistogram[i];
	            }	            	            
            }
            catch(Exception e){
            	e.printStackTrace();	//fields don't exist
            }
           
            }
            
        	PrintWriter writer = new PrintWriter("Results-Parsing.txt");
            for(int i = 0; i < NUM_CELLS; i++){
            	histogram[i] /= numOccurences[i];
            	writer.println("[" + i +"] : " + histogram[i] + " occs: " + numOccurences[i]);
            	System.out.println("[" + i +"] : " + histogram[i]);
            }
        	writer.close();
    	}
         catch (UnknownHostException e) {
		//e.printStackTrace();
	} catch (IOException ex) {
            Logger.getLogger(RetrieveMp7.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


