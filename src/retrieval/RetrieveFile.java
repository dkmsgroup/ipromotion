/*******************************************************************************
 * Copyright (C) 2014 Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package retrieval;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

     
/**
 *
 * @author Giorgos
 */


public class RetrieveFile {
	
	private static final String server = "Localhost";
	private static final int port = 27017;
	private static final String dbName  = "Savage";
	private static DB db;
	private static String bucketName = "Savage";
	
	
	private String createQuery(String key, String Value){
		
		if(key.equals("filename"))
			return "{ filename : " + Value + " } ";
		if(key.equals("id"))
			return "{ _id : " + Value + " }"; 
		else{
			throw new IllegalArgumentException();
		}
	}
	
	
	private List<GridFSDBFile> getFileById(String id){
		String query = createQuery("id", id);
        GridFS gfsZip = new GridFS(db, bucketName);
        List<GridFSDBFile> ZipForOutput = gfsZip.find(query);
        return ZipForOutput;
	}
	

	private List<GridFSDBFile> getFileByName(String name){
		String query = createQuery("filename", name);
        GridFS gfsZip = new GridFS(db, bucketName);
        List<GridFSDBFile> ZipForOutput = gfsZip.find(query);
        return ZipForOutput;
	}
	
	
	public DB getDB() throws UnknownHostException{
		
		if(db == null){
			 Mongo mongo = new Mongo(server, 27017);
	         db = mongo.getDB(dbName);
		}
		
		return db;
		
	}
	
	
    public static void main(String[] args){
    	
    	try {
           
            RetrieveFile getFile = new RetrieveFile();
            String newFileName = args[0].substring(args[0].lastIndexOf('/')+1, args[0].lastIndexOf('.'));
            //Start time measurement
            long startTime = System.currentTimeMillis();
 
            (getFile.getFileByName(newFileName)).get(0).writeTo(new File("/tmp/zips/" + newFileName));

            long stopTime = System.currentTimeMillis();

            
            long elapsedTime = stopTime - startTime;
            System.out.println(elapsedTime);
            
        	}
         catch (UnknownHostException e) {
        	 e.printStackTrace();
         } catch (IOException ex) {
            Logger.getLogger(RetrieveFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
