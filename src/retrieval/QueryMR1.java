/*******************************************************************************
 * Copyright (C) 2014 Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package retrieval;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.MongoClient;

public class QueryMR1 {

	private String dbName;
	private DB db;
	private MongoClient mongoClient;
	
	private static final String map1 = "function(){"
    		+ "try{"
    		+ "var length = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection.length;"
    		+ "var i = 0;"
    		+ "var toEmit;"
    		+ "var UNDERFLOW  = 0.000000001;"
    		+ "var OVERFLOW  = 10000000;"
    		+ "var toEmit2 = [];"
    		+ "for(var j = 0; j < length; j++){"
    			+"try {"
    			+ "var v1 = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[0].BoundingBox3DSize.BoxHeight;"
    			+ "var v2 = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[0].BoundingBox3DSize.BoxDepth;"
    			+ "var v3 = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[0].BoundingBox3DSize.BoxWidth;"
    			+ "var volume = v1*v2*v3;"
    			+ "if(volume < UNDERFLOW || volume > OVERFLOW)"
    			+ "	volume = 0;"
    			+ "var RGB = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[1].Geometry3D.DominantColor3D.Value.Index;"
    			+ "var colors = RGB.split(' ');"
    			+ "var dim1 = Math.floor(colors[0] / 32);"
    			+ "var dim2 = Math.floor(colors[1] / 32);"
    			+ "var dim3 = Math.floor(colors[2] / 32);"
    			+ "var cell = dim3 * 64 + dim2 * 8 + dim1;"
    			+ "toEmit =  { cell: cell, volume: volume};"
    			+ "if(length == 2){"
    			+ "		toEmit2.push(toEmit);"
    			+ "		if(!isNaN(toEmit2.volume))"
    			+ "			emit(this.filename, toEmit2);"
    			+ "		toEmit2.pop();"
    			+ "	}"
    			+ "else{" 
    			+ "		if(!isNaN(toEmit.volume))"
    			+ "			emit(this.filename, toEmit);"
    			+ "	}"
    			+ "}catch(err){"
//    			+ "		print(err);"
    			+ "}"
    			+ "}"
    			+ "}"
    			+ "catch(err2){"
   // 			+ "		print(err2);"
    			+ "}"	        			
    			+ "};";
	
	
	private static final String reduce1 = "function(k, values) {"
	        		+ "var sum = 0;"
	        		+ "var maxINT = 100000000;"
	        		+ "var flag = 0;"
	        		+ "var vals = [];"
	      //  		+ "print(\"Executing reduce 1\");"
	        		+ "for (var i = 0; i < values.length; i++){"
	        		+ "	vals.push(values[i]);"
	        		+ "}"
	        		+ "for (var i = 0; i < values.length; i++){"
	        		+ "		if( sum + vals[i].volume > maxINT){"
	      //  		+ "			print(\"Potential Overflow\");"
	        		+ "			sum = maxINT;"
	        		+ "		}"
	        		+ "		else"
	        		+ "			sum += vals[i].volume;"
	        		+ "}"
	        		+ "	for (var i = 0; i < values.length; i++){"
	        		+ "		vals[i].volume = vals[i].volume / sum;"
	        		+ "		if(vals[i].volume <= 0 || vals[i].volume >= 1){"
	        		+ "			flag = 1;"
	        		+ "		}"
	        		+ "	 } "
	        		+ "var ret = {v : values} ;"
	        		+ "if(flag !== 0)"
	        		+ "		print(\"Something went wrong\");"
	        		+ "return (k, ret);"
	        		+ "};";
	
	private static final String map2 = "function(){"
    		+ "try{"
    		+ "var OVERFLOW = 1;"
    		+ "if (typeof(this.value.v) !== 'undefined'){"
    //		+ "		print(\"value.v\");"
    		+ "		length = this.value.v.length;"
    		+ "		for(var j = 0; j < length; j++){"
    		+ "			if(this.value.v[j].volume >= 0 && this.value.v[j].volume <= 1)"
    		+ "				emit(this.value.v[j].cell, this.value.v[j].volume);"	
    		+ "		}"
    		+ "	}"
    		+ "else{"
   // 		+ "		print(\"value.volume\");"
    //		+ "			if(this.value.volume >= 0 && this.value.volume <= 1)"
    //		+ "			emit(this.value.cell, this.value.volume);"
    		+ "	}"
    		+ "}"
    		+ "catch(err){"
    	//	+ "print(err);"
    		+ "}"
    		+ "};";
			
	
	//eksago gia kathe keli to average pososto tou	
	private static final String reduce2 = "function(k, values) {"
	        		+ "var sum = 0;"
	        		+ "var count = 0;"
	        		+ "var MAX_INT = 100000000;"
	        		+ "for (var i = 0; i < values.length; i++){"
	        		+ "	if(!isNaN(values[i])){"

	        		+ "		if( sum + values[i] > MAX_INT){"
	    //    		+ "			print(\"Potential Overflow\");"
	        		+ "			sum = max_INT;"
	        		+ "		}"
	        		+ "		else{"
	        		+ "			sum += values[i];"
	        		+ "		}"
	        		+ "		count++;"
	        		+ "	}"
	        		+ "}"
	        		+ "return (k, sum / count);"
	        		+ "};";
	
	
	
	private static final String outputCollection1 = "out1";
	private static final String outputCollection2 = "out2";

	
	private void executeQuery(BasicDBObject searchQuery, String collectionName, String outputCollection ,String map, String reduce) throws FileNotFoundException{
	        
	        MapReduceCommand mr = new MapReduceCommand(db.getCollection(collectionName), map, reduce, outputCollection, MapReduceCommand.OutputType.REPLACE, searchQuery);
	      //  mr.setFinalize(finalize1);
	        MapReduceOutput out = db.getCollection(collectionName).mapReduce(mr);
	        for (DBObject o : out.results()) {
	        	System.out.println(o.toString());
	        }	        
		}
	
	public QueryMR1(MongoClient mongoClient, String dbName){
		this.dbName = dbName;
		this.mongoClient = mongoClient;
        this.db = mongoClient.getDB(dbName);
	}
	
	public static void main(String[] args) throws Exception{
	        
	        MongoClient mongoClient = new MongoClient("localhost", 27017);	 
	        
	        QueryMR1 mc = new QueryMR1(mongoClient, "Savage");
	        
	        long startTime = System.currentTimeMillis();
	        BasicDBObject searchQuery = new BasicDBObject();
	        //searchQuery.
	       // searchQuery.put("filename", "Bear-Russia1");
	        mc.executeQuery(searchQuery, "Savage.files", outputCollection1, map1, reduce1);
	        mc.executeQuery(searchQuery, outputCollection1, outputCollection2, map2, reduce2);

	        long endTime = System.currentTimeMillis();
	        System.out.println(endTime - startTime);
	}
}
