/*******************************************************************************
 * Copyright (C) 2014 Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package retrieval;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;


public class CreateZips {

    private static String savageHome = "C:\\Diplomatikh\\Savage";
    private static String zipsHome = "C:\\Diplomatikh\\zipse";

    public static void main(String args[]){
        //inputPath: Savage/AircraftFixedWing/AV8B-Harrier-UnitedStates/[mp7_name].mp7
        //     homeFolder/lvl1/lvl2
        //outputPath: zips/AircraftFixedWing/AV8B-Harrier-UnitedStates/AV8B-Harrier-UnitedStates[1..9]/AV8B-Harrier-UnitedStates1.zip and AV8B-Harrier-UnitedStates1_[mp7_name]
        File savageHomeFolder = new File(savageHome);
        for(File lvl1 : savageHomeFolder.listFiles()){
            for(File lvl2 : lvl1.listFiles()){
                       int i = 0;
                if(lvl2.listFiles() != null){
                for(File mp7File : lvl2.listFiles()){
                    if(mp7File.getName().endsWith(".mp7")){
                        System.out.println("Found mp7\n");
                        i++;
                        String inputFolderPath = lvl2.getPath();
                        String inputMP7Name = mp7File.getName();
                        String outputZipPath = zipsHome + "/" + lvl1.getName() + "/" + lvl2.getName() + "/" + lvl2.getName() + i;
                        String outputZipName = lvl2.getName() + i + ".zip";
                        //LocalTransformer.main(new String[]{inputX3DPath, inputX3DName, outputX3DPath});
                        System.out.println(inputFolderPath + ", " + inputMP7Name + ", " + outputZipPath);
                        File zipFile = new File(outputZipPath);
                        if(!zipFile.exists()){
                            zipFile.mkdirs();
                        }
                        ZipHelper.main(new String[]{inputFolderPath, outputZipPath + "/" + outputZipName});
                        File sourceMp7 = new File(inputFolderPath + "/" + inputMP7Name);
                        File dest = new File(outputZipPath);
                        try {
                            FileUtils.copyFileToDirectory(sourceMp7, dest);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        //arg0 = inputX3D, arg1 = inputX3Dname, arg2 = outputPath
                    }
                }
            }
            }
        }
    }
 }
