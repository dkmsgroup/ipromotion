/*******************************************************************************
 * Copyright (C) 2014 Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package retrieval;



import java.io.File;

import org.json.JSONException;

public class PopulateMongo {

    private static String zipsHome = "C:\\Diplomatikh\\zips";
    
    public static void main(String args[]) throws JSONException{
        //path: zips/AircraftFixedWing/AV8B-Harrier-UnitedStates/AV8B-Harrier-UnitedStates1/AV8B-Harrier-UnitedStates1.zip
        //     zipsHome/lvl1/lvl2/lvl3/zipFile + mp7File
        //Ta paths eksartontai apo to pos tha zipparo ta arxeia, diladh to deftero scrip
        File zipsHomeFolder = new File(zipsHome);
        for(File lvl1 : zipsHomeFolder.listFiles()){    //AircraftFixedWing etc
            for(File lvl2 : lvl1.listFiles()){  //AV8B-Harrier-UnitedStates etc
                for(File lvl3 : lvl2.listFiles()){ //AV8B-Harrier-UnitedStates1 etc
                    String path = lvl3.getPath();
                    Create_Buckets.main(new String[]{path});
                    //LocalTransformer.main(new String[]{inputX3DPath, inputX3DName, outputX3DPath});
                    }
                }
            }
        }
}