/*******************************************************************************
 * Copyright (C) 2014 Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package retrieval;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.mongodb.util.JSON.*;
import java.nio.file.Path;



/**
 *
 * @author George
 */

/*Dino os orisma to path tou zip kai dhmiourgeitai to antistoixo bucket sto gridfs */

public class Create_Buckets {
	
	private static final String dbName = "Savage";
	private static final String LOCALHOST = "Localhost";
	private static final String MP7 = "mp7";
	private static final String ZIP = "zip";
	private static final String collectionName = "Savage.files";
	
    public static void main(String[] args) throws JSONException {
            	
     	try {
            Mongo mongo = new Mongo(LOCALHOST, 27017);
            DB Mydb = mongo.getDB(dbName);
        
            String Path = args[0];
            int index = Path.lastIndexOf('\\');
            String newFileName = Path.substring(index+1);	
            String mp7File="";
            
            for(File f: new File(Path).listFiles()){
                if(f.getName().endsWith(MP7))
                    mp7File =  f.getPath();
            }
            
            File zipFile = new File(Path + "//" + newFileName+"."+ ZIP);
            System.out.println("File: " + Path + newFileName + "." + ZIP + "\n " + MP7 + " " + mp7File);
            GridFS gfs = new GridFS(Mydb, dbName);
            GridFSInputFile gfsFile = gfs.createFile(zipFile);
            gfsFile.setFilename(newFileName);
            gfsFile.save();							//Apothikevo to arxeio
            
            /*
            *Eisago to mp7 sta metadata tou arxeiou se morfh json, stin files collection
            */
       
            
            /*testing with reading xml as a string*/
            String text = new Scanner( new File(mp7File) ).useDelimiter("\\A").next();
            System.out.println(""+text);
            JSONObject xmlJSONObj = XML.toJSONObject(text);
            BasicDBObject toAppend = new BasicDBObject();
            
            Object o = com.mongodb.util.JSON.parse(xmlJSONObj.toString());
            DBObject dbObj = (DBObject) o;
            
            toAppend.append("$set", new BasicDBObject().append("metadata", o));
            DBCollection c = Mydb.getCollection(collectionName);
        
            BasicDBObject query = new BasicDBObject();
            query.append("filename", newFileName); 
            c.update(query, toAppend);        
        }
         catch (UnknownHostException e) {
		e.printStackTrace();
	} catch (MongoException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
    }
}