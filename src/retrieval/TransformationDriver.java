/*******************************************************************************
 * Copyright (C) 2014 Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
package retrieval;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author root
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import org.exist.xquery.modules.mpeg7.LocalTransformer;

public class TransformationDriver {

    
    // /home/ntua/Diplomatiki/Savage   to kanoniko
    private static final String C = "C:\\";
    private static final String savageHome = "C:\\Diplomatikh\\Savage";
    private static File DiplomatikhHomeFolder;
    
    public static void main(String args[]) throws IOException{
        //path: Savage/AircraftFixedWing/AV8B-Harrier-UnitedStates/AV8B-Harrier-UnitedStates.x3d
        //     homeFolder/lvl1/lvl2/x3dFile
        File savageHomeFolder = new File(savageHome);
        File CFolder = new File(C);
        
        PrintStream out = new PrintStream(new FileOutputStream("C:\\Diplomatikh\\output.txt"));
        System.setOut(out);
        
        for(File ff : CFolder.listFiles())
            if(ff.getName().equals("Diplomatikh"))
                DiplomatikhHomeFolder = new File(ff.getPath());

        for(File ff : DiplomatikhHomeFolder.listFiles())
            if(ff.getName().equals("Savage"))
                savageHomeFolder = new File(ff.getPath());
        
        System.out.println(""+savageHomeFolder.getPath());
        for(File lvl1 : savageHomeFolder.listFiles()){
            if(!lvl1.getName().equals("Tools"))
                continue;
          //  System.out.println(""+lvl1.getPath());
            for(File lvl2 : lvl1.listFiles()){
                            if(lvl2.getName().equals("Animation"))
                                continue;

          //      System.out.println(""+lvl2.getPath());
                if(lvl2.listFiles() != null){
                    for(File x3dFile : lvl2.listFiles()){
               //                                     System.out.println(""+x3dFile.getName());
                        if(x3dFile.getName().endsWith(".x3d")){
                            System.out.println(""+x3dFile.getPath());
                            String inputX3DPath = lvl2.getPath();
                            String inputX3DName = x3dFile.getName();
                            String outputX3DPath = lvl2.getPath();
                            try{
                                LocalTransformer.main(new String[]{inputX3DPath+"\\", inputX3DName, outputX3DPath+"\\"});
                            }
                            catch(Exception ex){
                                ex.printStackTrace();;
                            }
                    //        System.out.println(inputX3DPath + ", " + inputX3DName + ", " + outputX3DPath);
                            //arg0 = inputX3D, arg1 = inputX3Dname, arg2 = outputPath
                        }
                   //     System.out.println("\n");

                      }
                }
              }
          }
      }
  }