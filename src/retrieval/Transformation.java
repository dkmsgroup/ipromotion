/*******************************************************************************
 * Copyright (C) 2014 Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import mpegMpeg7Schema2001.Mpeg7Document;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Transformation {

    private static Logger logger = Logger.getLogger(Transformation.class);

    public static void main(String[] args) throws IOException, SAXException{
       
        File inXml = new File(args[0]);

        //*********************************************************
        //ARXH KODIKA GIA TO EXTRACT TON POINTS ENOS INDEXEDFACESET
        //*********************************************************
        String[] tokenizedCoord = null;
        String[] tokenizedPoint = null;

        String crossSectionAttrib = null;
        String spineAttrib = null;
        String scaleAttrib = null;
        String orientAttrib = null;

        List totalPointParts = null;
        File file = null;
        BufferedWriter bw = null;
        ArrayList<String> resultedIFSExtractionList = new ArrayList();  //mine
        ArrayList<String> resultedExtrExtractionList = new ArrayList();
        ArrayList<String> resultedExtrBBoxList = new ArrayList();
        StringBuilder IFSStringBuilder = new StringBuilder();
        StringBuilder ExtrShapeStringBuilder = new StringBuilder();
        StringBuilder ExtrBBoxStringBuilder = new StringBuilder();

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inXml);
            doc.getDocumentElement().normalize();

            //if (doc.getElementsByTagName("IndexedFaceSet") != null && doc.getElementsByTagName("IndexedFaceSet").getLength() > 0) {
            int shapeCount = 0;
            int extrCount = 0;
            int ifsCount = 0;

            NodeList shpList = doc.getElementsByTagName("Shape");
            for (int temp = 0; temp < shpList.getLength(); temp++) {
                shapeCount += 1;
                Node nNode = shpList.item(temp);
                //System.out.println("Number of Shape in document: " + temp);
                Element eElement = (Element) nNode;
                //System.out.println("eElement= " + eElement);
                if (eElement.getElementsByTagName("IndexedFaceSet").item(0) != null) {
                    ifsCount += 1;
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element ifsElem = (Element) eElement.getElementsByTagName("IndexedFaceSet").item(0);
                        String coordIndexAttrib = ifsElem.getAttribute("coordIndex");

                        //System.out.println("IndexedFaceSet coordIndex= " + coordIndexAttrib);
                        int lastMinusOnePosition = coordIndexAttrib.lastIndexOf("-1");
                        //System.out.println(lastMinusOnePosition);
                        if (lastMinusOnePosition != -1) {
                            String stringWithoutLastMinusOne = coordIndexAttrib.substring(0, lastMinusOnePosition);
                            tokenizedCoord = stringWithoutLastMinusOne.split(" -1 ");
                        } else {
                            tokenizedCoord = new String[1];
                            tokenizedCoord[0] = coordIndexAttrib;
                        }
                        //System.out.println(stringWithoutLastMinusOne);

                        /*
                         for (int i = 0; i < tokenizedCoord.length; i++) {
                         System.out.println(tokenizedCoord[i]);
                         }
                         */
                        Element coordElem = (Element) eElement.getElementsByTagName("Coordinate").item(0);
                        String pointAttrib = coordElem.getAttribute("point");
                        //System.out.println("Coordinate point= " + pointAttrib);
                        tokenizedPoint = pointAttrib.split(" ");
                        totalPointParts = new ArrayList();
                        if (tokenizedPoint.length > 2) {
                            for (int i = 0; i < tokenizedPoint.length; i = i + 3) {
                                String pointPart = tokenizedPoint[i].concat(" " + tokenizedPoint[i + 1] + " " + tokenizedPoint[i + 2]);
                                //System.out.println(pointPart);
                                //System.out.println("start of next triada: " + tokenizedPoint[i]);
                                totalPointParts.add(pointPart);
                            }
                        } else {
                            // Unique case: an IndexedFaceSet without points (authoring error!)
                            totalPointParts.add("0 0 0");
                            totalPointParts.add("0.0000001 0 0");
                            totalPointParts.add("0 0.0000001 0");
                            
                            tokenizedCoord=new String[1];
                            tokenizedCoord[0]="0 1 2";
                        }


                        /*
                         for (int i = 0; i < totalPointParts.size(); i++) {
                         System.out.println("No" + i + " triada: " + totalPointParts.get(i));
                         }
                         */
                    }
                    String coordTempFile = writeCoordIndexToFile(tokenizedCoord, file, bw);
                    String pointTempFile = writePointToFile(totalPointParts, file, bw);
                    String[] tempFiles = {coordTempFile, pointTempFile};
                    String resultedExtraction = ShapeIndexExtraction.shapeIndexEctraction(tempFiles);
                    resultedIFSExtractionList.add(resultedExtraction);

                } else if (eElement.getElementsByTagName("Extrusion").item(0) != null) {
                    extrCount += 1;
                    crossSectionAttrib = null;
                    spineAttrib = null;
                    scaleAttrib = null;
                    orientAttrib = null;
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element extrElem = (Element) eElement.getElementsByTagName("Extrusion").item(0);
                        crossSectionAttrib = extrElem.getAttribute("crossSection");
                        //System.out.println(crossSectionAttrib);
                        spineAttrib = extrElem.getAttribute("spine");
                        //System.out.println(spineAttrib);
                        if (extrElem.getAttribute("scale") != null) {
                            scaleAttrib = extrElem.getAttribute("scale");
                        }
                        //System.out.println(scaleAttrib);
                        if (extrElem.getAttribute("spine") != null) {
                            orientAttrib = extrElem.getAttribute("orientation");
                        }
                        //System.out.println(orientAttrib);
                    }
                    file = new File("Extrusion.txt");
                    String coordTempFile = writeExtrusionParamsToFile(crossSectionAttrib, spineAttrib, scaleAttrib, orientAttrib, file, bw);

                    String[] ExtrTempFile = {coordTempFile};
                    String resultedExtraction = ExtrusionDescription.ExtrusionDescription(ExtrTempFile);
                    file.delete();

                    String ExtrBBox = resultedExtraction.substring(0, resultedExtraction.indexOf("&") - 1);
                    String ExtrShape = resultedExtraction.substring(resultedExtraction.indexOf("&") + 1);

                    //System.out.println(ExtrBBox);
                    //System.out.println(ExtrShape);
                    resultedExtrBBoxList.add(ExtrBBox);
                    resultedExtrExtractionList.add(ExtrShape);

                } else {
//                    System.out.println("oeo");
                }
            }
            //System.out.println(shapeCount + " " + ifsCount + " " + extrCount);
            //System.out.println(resultedIFSExtractionList.size() + " " + resultedExtrExtractionList.size() + " " + resultedExtrBBoxList.size());

            /*
             for (int i = 0; i < resultedExtractionList.size(); i++) {
             System.out.println(i + ": " + resultedExtractionList.get(i));
             }
             */
            for (int i = 0; i < resultedIFSExtractionList.size(); i++) {
                IFSStringBuilder.append(resultedIFSExtractionList.get(i));
                IFSStringBuilder.append("#");
                //System.out.println(IFSStringBuilder);
            }

            for (int i = 0; i < resultedExtrExtractionList.size(); i++) {
                ExtrShapeStringBuilder.append(resultedExtrExtractionList.get(i));
                ExtrShapeStringBuilder.append("#");

                ExtrBBoxStringBuilder.append(resultedExtrBBoxList.get(i));
                ExtrBBoxStringBuilder.append("#");

                //System.out.println(IFSStringBuilder);
            }

            //System.out.println("Synoliko String: " + IFSStringBuilder.toString());
        } catch (ParserConfigurationException e) {
        }

        //**********************************************************
        //TELOS KODIKA GIA TO EXTRACT TON POINTS ENOS INDEXEDFACESET
        //**********************************************************
        
        
        // 1. Instantiate a TransformerFactory.
        TransformerFactory factory = TransformerFactory.newInstance();
        StreamSource xslStream = new StreamSource(Transformation.class.getResourceAsStream("x3d_to_mpeg7_transform.xsl"));
        // 2. Use the TransformerFactory to process the stylesheet Source and generate a Transformer.
        Transformer transformer = null;
        try {
            transformer = factory.newTransformer(xslStream);
        } catch (TransformerConfigurationException exXSL) {
            //System.out.println(getServletContext().getRealPath("/"));
            //System.setProperty("resultFolder", getServletContext().getRealPath("/"));
            //System.out.println(System.getProperty("resultFolder"));
            logger.log(Priority.FATAL, null, exXSL);
            //Logger.getLogger(TransformationServlet.class.getName()).log(Level.SEVERE, null, exXSL);
        }
        transformer.setErrorListener(new MyErrorListener());
        StreamSource in = new StreamSource(inXml);
        // 3. Use the Transformer to transform an XML Source and send the output to a Result object.
        //xmlSystemId is used to pass the full path of the xml file currently being transformed through the xsl
        String xmlSystemId = inXml.toURI().toString();
        transformer.setParameter("filename", xmlSystemId);
        transformer.setParameter("pointsExtraction", IFSStringBuilder.toString());
        transformer.setParameter("extrusionPointsExtraction", ExtrShapeStringBuilder.toString());
        transformer.setParameter("extrusionBBoxParams", ExtrBBoxStringBuilder.toString());
        String fileNameOnly = org.apache.commons.io.FilenameUtils.removeExtension(inXml.getName());
        File mp7 = new File(inXml.getParent() + "/" + fileNameOnly + ".mp7");
        String outputXML = mp7.toURI().toString();
        try {
            transformer.transform(in, new StreamResult(outputXML));
            System.out.println("TRANSFORMED SUCCESSFULLY!");
            validate(mp7);
        } catch (TransformerException exTransform) {
            System.out.println("TRANSFORMATION ERROR! The full stack trace of the root cause is available in the logs of Apache Tomcat.");
            logger.log(Priority.ERROR, null, exTransform);
            //Logger.getLogger(TransformationServlet.class.getName()).log(Level.SEVERE, null, exTransform);
        }

    }

    public static String writeCoordIndexToFile(String[] dataToWrite, File tempFile, BufferedWriter writer) {
        try {
            tempFile = new File("coordIndex.txt");
            // if file doesnt exists, then create it
            if (!tempFile.exists()) {
                tempFile.createNewFile();
            }
            writer = new BufferedWriter(new FileWriter(tempFile));
            for (int i = 0; i < dataToWrite.length; i++) {
                writer.write(dataToWrite[i]);
                writer.newLine();
            }
            writer.close();
            //System.out.println("SUCCESS!!!" + tempFile.getAbsolutePath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return tempFile.getAbsolutePath();
    }

    public static String writePointToFile(List totalPointParts, File tempFile, BufferedWriter writer) {
        try {
            tempFile = new File("point.txt");
            // if file doesnt exists, then create it
            if (!tempFile.exists()) {
                tempFile.createNewFile();
            }
            writer = new BufferedWriter(new FileWriter(tempFile));
            for (int i = 0; i < totalPointParts.size(); i++) {
                writer.write(totalPointParts.get(i).toString());
                writer.newLine();
            }
            writer.close();
            //System.out.println("SUCCESS!!! " + tempFile.getAbsolutePath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return tempFile.getAbsolutePath();
    }

    public static String writeExtrusionParamsToFile(String crossSectionAttrib, String spineAttrib, String scaleAttrib, String orientAttrib, File tempFile, BufferedWriter writer) {
        try {
            // if file doesnt exists, then create it
            if (tempFile.exists()) {
                tempFile.delete();
            }
            tempFile.createNewFile();
            writer = new BufferedWriter(new FileWriter(tempFile));
            writer.write("CrossSection");
            writer.newLine();
            writer.write(crossSectionAttrib);
            writer.newLine();
            writer.write("Spine");
            writer.newLine();
            writer.write(spineAttrib);
            writer.newLine();
            if (spineAttrib != null) {
                writer.write("Scale");
                writer.newLine();
                writer.write(scaleAttrib);
                writer.newLine();
            }
            if (orientAttrib != null) {

                writer.write("Orientation");
                writer.newLine();
                writer.write(orientAttrib);
                writer.newLine();
            }
            writer.close();
            //System.out.perintln("SUCCESS!!! " + tempFile.getAbsolutePath());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return tempFile.getAbsolutePath();
    }

public static boolean validate(File x3dinput) throws IOException{

    //Set up the validation error listener.
    ArrayList validationErrors = new ArrayList();
    XmlOptions xmlOptions = new XmlOptions();
    xmlOptions.setErrorListener(validationErrors);
    //Bind the instance to the generated XMLBeans types. Parse the transformed document 
    //into a Mpeg7 specific document to be able to use all the Mpeg-7 capabilitities
    Mpeg7Document myDoc = null;
    try {
        myDoc = Mpeg7Document.Factory.parse(x3dinput);
    } catch (XmlException exXML) {
        System.out.println("ERROR PARSING MPEG7! The full stack trace of the root cause is available in the logs of Apache Tomcat.");
        logger.log(Priority.FATAL, null, exXML);
        //Logger.getLogger(ValidationServlet.class.getName()).log(Level.SEVERE, null, exXML);
    }
    //customize the xmlOptions used to create//parse the xml file
    xmlOptions.setSaveNamespacesFirst();
    xmlOptions.setSavePrettyPrint();
    xmlOptions.setSavePrettyPrintIndent(3);
    xmlOptions.setSaveAggressiveNamespaces();
    final Map<String, String> implicitNamespaces = new HashMap();
    implicitNamespaces.put("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    xmlOptions.setSaveImplicitNamespaces(implicitNamespaces);
    xmlOptions.setUseDefaultNamespace();
    //The XML to be validated
    //String output = myDoc.xmlText(xmlOptions);
    myDoc.save(x3dinput);
    // During validation, errors are added to the ArrayList for
    // retrieval and printing by the printErrors method.
    boolean isValid = myDoc.validate(xmlOptions);
    // Print the errors if the XML is invalid.
    if (!isValid) {
        System.out.println("INVALID DESCRIPTION!");
        System.out.println("");
        Iterator iter = validationErrors.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
            System.out.println("");
        }
    }
    else
    {
        System.out.println("DESCRIPTION IS VALID!");
    }
    
    return isValid;
}
}

class MyErrorListener implements ErrorListener {

    private static Logger logger = Logger.getLogger(MyErrorListener.class);

    @Override
    public void warning(TransformerException e) throws TransformerException {
        logger.log(Priority.WARN, null, e);
        //show("Warning", e);
        //throw (e);
    }

    @Override
    public void error(TransformerException e) throws TransformerException {
        logger.log(Priority.ERROR, null, e);
        //show("Error", e);
        //throw (e);
    }

    @Override
    public void fatalError(TransformerException e) throws TransformerException {
        logger.log(Priority.FATAL, null, e);
        //show("Fatal Error", e);
        //throw (e);
    }

    private void show(String type, TransformerException e) {
        System.out.println(type + ": " + e.getMessage());
        if (e.getLocationAsString() != null) {
            System.out.println(e.getLocationAsString());

        }
    }
}
