/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeyValue;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONException;

/**
 *
 * @author Administrator
 */
public class SearchKeyValuePath {
    
    File resultsFile;
    DBCollection collection;
    DB db;
    BasicDBObject query;
    
    public void Search() throws JSONException, IOException{
        
        DBCursor cursor = collection.find(query);
        
        while (cursor.hasNext()) {
            
            BasicDBObject nextDoc = (BasicDBObject) cursor.next();
            
            try (FileWriter fileWriter = new FileWriter(resultsFile, true)) {
                    fileWriter.write(nextDoc.get("name") + "\n");
                }
        }
    }
    
    /*
     * Constructor
     */
    
    public SearchKeyValuePath(String path, String resultsFileName, DB database, BasicDBObject qry) throws IOException{
        
        /*
         * Results written to path + "KeyValue\\results\\Path\\" folder.
         */
        
        String resultsSubPath = "KeyValue\\";
        File jsonSubFolder = new File(path + resultsSubPath);
        
        if(!jsonSubFolder.exists())
            jsonSubFolder.mkdir();
        
        resultsSubPath = "KeyValue\\results\\";
        jsonSubFolder = new File(path + resultsSubPath);
        
        if(!jsonSubFolder.exists())
            jsonSubFolder.mkdir();
        
        resultsSubPath = "KeyValue\\results\\Path\\";
        String resultsFolderPath = path + resultsSubPath;
        
        File resultsFolder = new File(resultsFolderPath);
        
        if(!resultsFolder.exists()){
            resultsFolder.mkdir();
        }
        
        /*
         * Compose name of results file.
         * Each query creates its own file.
         * If a file with the same name exists, overwrite it.
         */
        
        resultsFileName += ".txt";
        String resultsFilePath = resultsFolderPath + resultsFileName;
        
        resultsFile = new File(resultsFilePath);
        
        if(!resultsFile.exists()){
            resultsFile.createNewFile();
        }
        else{
            try (FileWriter fileWriter = new FileWriter(resultsFile)) {
                fileWriter.flush();
            }
        }
        
        String colName = "KeyValue";
        db = database;
        collection = db.getCollection(colName);
        query = qry;
        
    }
}
