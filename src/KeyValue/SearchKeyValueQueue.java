/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeyValue;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Administrator
 */
public class SearchKeyValueQueue {
    String elementName;
    String attributeName;
    Object attributeValue;
    
    File resultsFile;
    DBCollection collection;
    DB db;
    
    Queue<JSONObject> myQueue = new LinkedList<>();
    
    /*
     * Gets a JSONObject. Takes all key/value pairs.
     * If key = attributeName checks the value.
     * If value is a String returns if value is equal to attributeValue.
     * If value is a JSONArray returns if it contains a String equal to attributeValue.
     */
    
    private boolean CheckForAttributeMatchJSONObject(JSONObject json) throws JSONException{
        
        JSONArray allKeys = json.names();
        Object curValue;
        
        for(int i=0; i<allKeys.length(); i++){
            if(allKeys.get(i).equals(attributeName)){
                
                curValue = json.get(attributeName);
                
                if(curValue.equals(attributeValue)){
                    return true;
                }
                
                else if(curValue instanceof JSONArray){
                    
                    JSONArray arrayValues = (JSONArray) curValue;
                    
                    for(int j=0; j<arrayValues.length(); j++){
                        if( arrayValues.get(j).equals(attributeValue)){
                            return true;
                        }
                        
                    }
                }
            }
        }
        
        return false;
    }
    
    /*
     * Takes a JSON Array.
     * Returns if it contains a child JSON Object,
     * with key = attributeName and value = attributeValue.
     */
    
    private boolean CheckForAttributrMatchJSONArray(JSONArray jsonArray) throws JSONException {
        
        JSONObject childJSON;
        
        for(int j=0; j<jsonArray.length(); j++){
            
            if (jsonArray.get(j) instanceof JSONObject){
                childJSON = jsonArray.getJSONObject(j);
                
                if(childJSON.has(attributeName)){
                    if (childJSON.get(attributeName).equals(attributeValue))
                        return true;
                }
            }
        }
        
        return false;
    }
    
    
    /*
     * Takes father JSON Object. Extracts child JSON Objects.
     * Adds child JSON Objects to Queue.
     */
    
    private void AddChildJSONObjsToQueue(JSONObject fatherJSON) throws JSONException{
        
        JSONObject childJSON;
        
        String childKey;
        Object childValue;
        
        JSONArray allKeys = fatherJSON.names();
        
        for(int i=0; i<allKeys.length(); i++){
            childKey = (String) allKeys.get(i);
            childValue = fatherJSON.get(childKey);
            
            childJSON = new JSONObject();
            childJSON.put(childKey, childValue);
            
            myQueue.add(childJSON);
            
        }
    }
    
    /*
     * Gets a JSONObject with one key/value pair where value is a JSON Array.
     * Adds to Queue key/value pairs where key is the original key
     * and values all values found in JSON Array.
     * 
     * {"key" : [JSONObject1, JSONObject2, JSONObject3]}
     * 
     * New JSONObjects added to Queue :
     * {"key":JSONObject1} , {"key":JSONObject2} and {"key":JSONObject3}
     */
    
    private void AddChildJSONArrToQueue(JSONObject fatherJSON) throws JSONException{
        
        String key = (String) fatherJSON.names().get(0);
        JSONArray array = fatherJSON.getJSONArray(key);
        
        JSONObject childJSON;
        String childKey = key;
        Object childValue;
        
        for(int i=0; i<array.length(); i++){
            
            childValue = array.get(i);
            
            childJSON = new JSONObject();
            childJSON.put(childKey, childValue);
            
            myQueue.add(childJSON);
        }
        
    }
    
    /*
     * Pulls all JSON Objects from Queue one at a time.
     * For each JSON Object gets key and value.
     * If value is a JSON Objects or JSON Array, adds child JSON Objects to Queue.
     * If key = elementName and a match hasn't been found yet,
     * checks the value for a match.
     */
    
    private boolean SearchQueue() throws JSONException{
        
        JSONObject json;
        String key;
        Object value;
        
        boolean found = false;
        
        while(!myQueue.isEmpty()){
            json = myQueue.poll();
            
            key = (String) json.names().get(0);
            value = json.get(key);
            
            if(value instanceof JSONObject )
                AddChildJSONObjsToQueue((JSONObject) value);
            else if (value instanceof JSONArray)
                AddChildJSONArrToQueue(json);
            
            if((key.equalsIgnoreCase(elementName)) && (found == false)){
                
                if(value instanceof JSONArray){
                    found = CheckForAttributrMatchJSONArray((JSONArray) value);
                    if (found == true){
                        myQueue.clear();
                        return true;
                    }
                }
                else if (value instanceof JSONObject){
                    found = CheckForAttributeMatchJSONObject((JSONObject) value);
                    if (found == true){
                        myQueue.clear();
                        return true;
                    }
                }
                
            }
        }
        
        return false;
    }
    
    private void SubmitQuery(JSONObject json) throws IOException, JSONException{
        boolean matchQuery;
        
        AddChildJSONObjsToQueue(json);
        matchQuery = SearchQueue();
        
        /*
         * If query returns true write result to results file.
         */
        
        if(matchQuery){
            try (FileWriter fileWriter = new FileWriter(resultsFile, true)) {
                    fileWriter.write(json.getString("name") + "\n");
                }
        }
        
    }
    
    public void Search() throws JSONException, IOException{
        DBCursor cursor = collection.find();
        
        while (cursor.hasNext()) {
            
            BasicDBObject nextDoc = (BasicDBObject) cursor.next();
            
            JSONObject jDoc = new JSONObject(nextDoc.toString());
            
            SubmitQuery(jDoc);
        }
    }
    
    
    /*
     * Constructor
     */
    
    public SearchKeyValueQueue(String elName, String atName, Object atValue, String path, String resultsFileName, DB database) throws IOException{
        elementName = elName;
        attributeName = atName;
        attributeValue = atValue;
        
        
        /*
         * Results written to path + "KeyValue\\results\\Queue\\" folder.
         */
        
        String resultsSubPath = "KeyValue\\";
        File jsonSubFolder = new File(path + resultsSubPath);
        
        if(!jsonSubFolder.exists())
            jsonSubFolder.mkdir();
        
        resultsSubPath = "KeyValue\\results\\";
        jsonSubFolder = new File(path + resultsSubPath);
        
        if(!jsonSubFolder.exists())
            jsonSubFolder.mkdir();
        
        resultsSubPath = "KeyValue\\results\\Queue\\";
        String resultsFolderPath = path + resultsSubPath;
        
        File resultsFolder = new File(resultsFolderPath);
        
        if(!resultsFolder.exists()){
            resultsFolder.mkdir();
        }
        
        /*
         * Compose name of results file.
         * Each query creates its own file.
         * If a file with the same name exists, overwrite it.
         */
        
        resultsFileName += ".txt";
        String resultsFilePath = resultsFolderPath + resultsFileName;
        
        resultsFile = new File(resultsFilePath);
        
        if(!resultsFile.exists()){
            resultsFile.createNewFile();
        }
        else{
            try (FileWriter fileWriter = new FileWriter(resultsFile)) {
                fileWriter.flush();
            }
        }
        
        String colName = "KeyValue";
        db = database;
        collection = db.getCollection(colName);
        
    }
}
