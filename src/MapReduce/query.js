/*******************************************************************************
 * Copyright (C) 2014  Georgios Tentes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
var mapFunction = 
	function() {		
	 try{
		 var length = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection.length;
		 var i = 0;
		 for(var j = 0; j < length; j){
			 try {
				 var v1 = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[0].BoundingBox3DSize.BoxHeight;
				 var v2 = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[0].BoundingBox3DSize.BoxDepth;
				 var v3 = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[0].BoundingBox3DSize.BoxWidth;
				 var volume = v1*v2*v3;
				 var RGB = this.metadata.Mpeg7.Description[1].MultimediaContent.StructuredCollection.Collection[1].DescriptorCollection[j].Descriptor[1].Geometry3D.DominantColor3D.Value.Index;
				 var colors = RGB.split(' ');
				 var dim1 = Math.floor(colors[0] / 32);
				 var dim2 = Math.floor(colors[1] / 32);
				 var dim3 = Math.floor(colors[2] / 32);
				 var cell = dim3 * 64  dim2 * 8  dim1;
				 var toEmit =  { cell: cell, volume: volume}; 
				 emit(this.Filename, toEmit);
			 }catch(err){
				 //error within the file, one of the descriptors doesn't have all the required fields. I continue emitting for the rest of the descriptors within the file
			 }
		 	}
		 }
		 catch(err2){
			 //the file doesn't have a DescriptorCollection array so we skip it.
		 	}	        			
		 };
		
var reduceFunction = 
	function(k, values) {
	 	var sumVolumes = 0;
	 	for (var i = 0; i < values.length; i){	
	 		sum = values[i].volume;
	 	} 
	 	for (var i = 0; i < values.length; i){	
		 	return (values[i].cell, values[i].volume / sum);
	 	} 
	};
	