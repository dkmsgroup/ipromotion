/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MapReduce;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.MongoClient;
import common.WriteResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author Administrator
 */
public class MapReduce {
    File resultsFile;
    String pathOfRes;
    String pathOfFunctions;
    
    /*
     * Create resultsFile with path = "pathOfRes".
     * If resultFile exists, flush it.
     */
    
    private void CreateResultsFile () throws IOException{
        resultsFile = new File(pathOfRes);
        if(!resultsFile.exists()){
            resultsFile.createNewFile();
        }
        else{
            try (FileWriter fileWriter = new FileWriter(resultsFile)) {
                fileWriter.flush();
            }
        }
    }
    
    /*
     * Convert inFile into a String. Return String.
     */
    
    private String GetFunctionString(File inFile) throws FileNotFoundException{
        
        String functionString;
        
        Scanner jsonContent = new Scanner(inFile);
        functionString = jsonContent.useDelimiter("\\Z").next();
        
        return functionString;
    }
    
    /*
     * Query documents in "MapReduce" collection. Select those that contain "child" and "count" fields.
     * If count of at least one document is greater that 0, repeat = true. Else repeat = false.
     */
    
    private boolean Done(DBCollection col){
        
        boolean repeat = false;
        
        BasicDBObject query = new BasicDBObject("value.child", new BasicDBObject("$exists", true));
        query.put("value.count", new BasicDBObject("$exists", true));
        
        DBCursor cursor = col.find(query);
        BasicDBObject nextDoc;
        Object count;
        
        while((cursor.hasNext()) && (repeat == false)){
            nextDoc = (BasicDBObject) cursor.next();
            
            count = ((DBObject)nextDoc.get("value")).get("count");
            
            if((double) count != 0){
                repeat = true;
            }
        }
        
        return repeat;
    }
    
    /*
     * Query all docs in "MapReduce" collection. Select fields "whole" and "color".
     * Sort them bu field "whole" in descending order.
     * Write to resultsFile with filePath = "pathRes" those with max value whole.
     */
    
    private void QueryOutput(DBCollection col) throws IOException{
        
        Object cur_whole, color, max;
        
        CreateResultsFile();
        
        BasicDBObject allDocs = new BasicDBObject();
        
        BasicDBObject fields = new BasicDBObject();
        fields.put("value.whole", 1);
        fields.put("value.color", 1);
        
        BasicDBObject orderBy = new BasicDBObject();
        orderBy.put("value.whole",-1);
        
        DBCursor cursor = col.find(allDocs, fields).sort(orderBy);
        BasicDBObject nextDoc;
        
        if (cursor.hasNext()){
            
            nextDoc = (BasicDBObject) cursor.next();
            
            max = ((DBObject)nextDoc.get("value")).get("whole");
            color = ((DBObject)nextDoc.get("value")).get("color");
            
            try (FileWriter fileWriter = new FileWriter(resultsFile, true)) {
                        fileWriter.write(nextDoc.get("_id") + " " + max + " " + color + "\n");
            }
            
            while (cursor.hasNext()) {
                nextDoc = (BasicDBObject) cursor.next();
                
                cur_whole = ((DBObject)nextDoc.get("value")).get("whole");
                color = ((DBObject)nextDoc.get("value")).get("color");
                
                if((double)cur_whole == (double)max){
                    try (FileWriter fileWriter = new FileWriter(resultsFile, true)) {
                            fileWriter.write(nextDoc.get("_id") + " " + cur_whole + " " + color + "\n");
                    }
                }
                else{
                    break;
                }
            }
        }
    }
    
    /*
     * Define map and reduce functions for SecondMapReduce. Merge results with data from "MapReduce" collection.
     * Second MapReduce is used to cross one layer of the trees at a time.
     * Each time objects of current layer submit their children updating their "count" and "cur_view" fields.
     * If at least one child BUT NOT LEAF object has count > 0, SecondMapReduce will be called again, examining next layer of the trees.
     */
    
    private DBCollection SecondMapReduce(String col, DB db) throws FileNotFoundException, IOException{
        
        // input collection same as output collection
        DBCollection input_collection = db.getCollection(col);
        String outputCol = col;
        DBCollection result_collection;
        // Create map String to be used as map function.
        File mapFile = new File(pathOfFunctions + "Map2_CrossTree.js");
        String map = GetFunctionString(mapFile);
        // Create reduce String to be used as reduce function.
        File reduceFile = new File(pathOfFunctions + "Reduce2_CrossTree.js");
        String reduce = GetFunctionString(reduceFile);
        
        // Build MapReduce command. Results will be MERGED with previous data of output collection.
        MapReduceCommand cmd = new MapReduceCommand(input_collection, map, reduce,
                     outputCol, MapReduceCommand.OutputType.MERGE, null);
        
        // Call MapReduce. Input from "MapReduce" collection, output to "MapReduce" collection,
        // using "Map2_CrossTree" and "Reduce2_CrossTree" functions.
        result_collection = input_collection.mapReduce(cmd).getOutputCollection();
        
        return (result_collection);
    }
    
    /*
     * Define map and reduce functions for FinalMapReduce. Replace data of "MapReduce" collection with results.
     * Final MapReduce is called to calculate the "whole" field of all lead objects.
     */
    
    private DBCollection FinalMapReduce(String col, DB db) throws FileNotFoundException{
        
        // input collection same as output collection
        DBCollection input_collection = db.getCollection(col);
        String outputColString = col;
        DBCollection result_collection;
        
        // Create map String to be used as map function.
        File mapFile = new File(pathOfFunctions + "Map3_CalcResult.js");
        String map = GetFunctionString(mapFile);
        
        // Create reduce String to be used as reduce function.
        File reduceFile = new File(pathOfFunctions + "Reduce3_CalcResult.js");
        String reduce = GetFunctionString(reduceFile);
        
        // Build MapReduce command. Results will REPLACE previous data of output collection.
        MapReduceCommand cmd = new MapReduceCommand(input_collection, map, reduce,
                     outputColString, MapReduceCommand.OutputType.REPLACE, null);
        
        // Call MapReduce. Input from "MapReduce" collection, output to "MapReduce" collection,
        // using "Map3_CalcResult" and "Reduce3_CalcResult" functions.
        result_collection = input_collection.mapReduce(cmd).getOutputCollection();
        
        return (result_collection);
    }
    
    /*
     * Define map and reduce functions for FirstMapReduce. Replace data of "MapReduce" collection with results.
     * First MapReduce is called to extract root objects of "KeyValue" collection.
     * root objects are NOT leafs and are NOT children of any other object.
     */
    
    private DBCollection FirstMapReduce(String inputColString, String outputColString, DB db) throws FileNotFoundException{
        
        // input collection
        DBCollection input_collection = db.getCollection(inputColString);//"KeyValue");
        // output collection
        String mapReduceColString = outputColString;//"MapReduce";
        DBCollection result_collection;
        
        // Create map String to be used as map function.
        File mapFile = new File(pathOfFunctions + "Map1_FindFather.js");
        String map = GetFunctionString(mapFile);
        
        // Create reduce String to be used as reduce function.
        File reduceFile = new File(pathOfFunctions + "Reduce1_FindFather.js");
        String reduce = GetFunctionString(reduceFile);
        
        // Build MapReduce command. Results will REPLACE previous data of output collection.
        MapReduceCommand cmd = new MapReduceCommand(input_collection, map, reduce,
                     mapReduceColString, MapReduceCommand.OutputType.REPLACE, null);
        
        // Call MapReduce. Input from "KeyValue" collection, output to "MapReduce" collection,
        // using "Map1_FindFather" and "Reduce1_FindFather" functions.
        MapReduceOutput callMapReduce = input_collection.mapReduce(cmd);
        result_collection = callMapReduce.getOutputCollection();
        
        return result_collection;
    }
    
    /*
     * Calls FirstMapReduce. Input Collection is "KeyValue", Output Collection is "MapReduce".
     * Calls SecondMapReduce until all layers of trees are accessed. Input Collection is "MapReduce", Output Collection is "MapReduce". Results are MERGED.
     * Calls FinalMapReduce. Input Collection is "MapReduce", Output Collection is "MapReduce". Results are REPLACED.
     * Calls QueryOutput to extract max values of "MapReduce" collection. Results written to file with filePath = pathOfRes.
     */
    
    private void CallMapReduce(DB db) throws IOException{
        
        String inputColString = "KeyValue";
        String mapReduceColString = "MapReduce";
        
        DBCollection result_collection;
        
        result_collection = FirstMapReduce(inputColString, mapReduceColString, db);
        
        // repeat is used to determine if SecondMapReduce should be called again.
        boolean repeat;
        
        // Call SecondMapReduce.
        result_collection = SecondMapReduce(mapReduceColString, db);
        repeat = Done(result_collection);
        
        while(repeat == true){
            
            result_collection = SecondMapReduce(mapReduceColString,db);
            repeat = Done(result_collection);
            
        }
        
        // Call FinalMapReduce.
        result_collection = FinalMapReduce(mapReduceColString, db);
        
        // Call QueryOutput.
        QueryOutput(result_collection);
    }
    
    /*
     * Create files to write results and duration. Call CallMapReduce method.
     */
    
    public void MapReduce(String demo, String times) throws UnknownHostException, IOException{
        long startTime, endTime, duration;
        WriteResult writeResult = new WriteResult();
        
        // Path of Project data.
        String bigPath = "C:\\Users\\Administrator\\Documents\\NetBeansProjects\\";
        
        // Path of Map/Reduce functions.
        pathOfFunctions = "C:\\Users\\Administrator\\Documents\\NetBeansProjects\\Project\\src\\MapReduce\\";
        
        // Subpath to write results (answers) of MapReduce.
        String resultsSubPath = "C:\\Users\\Administrator\\Documents\\NetBeansProjects\\MapReduce\\";
        
        // Check if "Results" folder exists.
        resultsSubPath += "Results\\";
        File tempFolder = new File(resultsSubPath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Build path to write results (answers) of MapReduce.
        pathOfRes = resultsSubPath + "demo" + demo + " application x" + times + ".txt";
        
        // Check if "Duration" folder exists.
        String durationSubPath = bigPath + "Duration\\";
        tempFolder = new File(durationSubPath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Check if "Duration//MapReduce" folder exists.
        String durationQueryPath = bigPath + "Duration\\MapReduce\\";
        tempFolder = new File(durationQueryPath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Path to write the duration of the MapReduce call.
        String durationQueryFilePath;
        // File to write the duration of the MapReduce call.
        File durationFile;
        
        String dbName = "demo" + demo + "x" + times;
        
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB db = mongoClient.getDB(dbName);
        
        startTime =  System.currentTimeMillis();
        
        CallMapReduce(db);
        
        endTime =  System.currentTimeMillis();
        duration = (endTime - startTime);
        
        durationQueryFilePath = durationQueryPath + "MapReduce demo" + demo + ".txt";
        durationFile = new File(durationQueryFilePath);
        
        writeResult.WriteResult(duration, times, durationFile);
        
        mongoClient.close();
    }
}
