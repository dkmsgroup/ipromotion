/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
// Purpose of map function is to calculate whole_view of leaf objects.
// whole = caur_view * norma

function map3_CalcResult(){
    
    var norma = this.value.norma;
    
    // If examined object is a leaf, it has norma, procceed.
    if(norma){
        var new_el = {
            whole : 0,
            color : {
                r : 0,
                g : 0,
                b : 0
            }
        };
        
        // Calculate whole, with only two decimal places.
        new_el.whole = Math.round((this.value.cur_view * this.value.norma) * 10)/10;
        
        // Add color attribute.
        new_el.color.r = this.value.color.r;
        new_el.color.g = this.value.color.g;
        new_el.color.b = this.value.color.b;
        
        // Emit leaf object.
        emit(this._id, new_el);
    }
}