/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MapReduce;

import MapReduce.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import org.json.JSONException;

/**
 *
 * @author Administrator
 */
public class CreateMapReduce {
    
    public static void main(String[] args) throws UnknownHostException, IOException, FileNotFoundException, JSONException{
        
        String demo = "12";
        String times;
        
        CreateMapReduceFiles create = new CreateMapReduceFiles();
        
        for(int i=1; i<=32; i*=2){
            times = "" + i + "";
            create.CreateMapReduceFiles(demo,times);
        }
    }
}
