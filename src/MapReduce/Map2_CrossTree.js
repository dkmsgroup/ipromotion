/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
// Purpose of map function is to cross tree.

function map2_CrossTree (){
    var count = this.value.count;
    var norma = this.value.norma;
    
    // If examined object is NOT a leaf, it has no norma, procceed.
    if(count && !norma){
        // Array of child objects.
        var children = this.value.child;
        // If count > 0, occurrence of examined object > 0, emit count times all child objects.
        if(count != 0){
            for(var i=0; i<count; i++){
                for(var j=0; j<children.length; j++){
                    // Extract child name.
                    var child_name = children[j].name;
                    // Built value, attributes, of child.
                    var value = {
                        cur_view : children[j].cur_view,
                        count : 1
                    };
                    // Emit child object.
                    emit(child_name, value);
                }
            }
        }
        // Also emit examined object, to maintain child array.
        var value = {
            cur_view : 0,
            count : 0,
            child : children
        };
        emit(this._id, value);
    }
    // If examined object is leaf, emit it.
    if(norma){
        emit(this._id, this.value);
    }
}