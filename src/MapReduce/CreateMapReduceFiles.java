/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MapReduce;

import KeyValue.CreateKeyValue;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import org.json.JSONException;

/**
 *
 * @author Administrator
 */
public class CreateMapReduceFiles {
    
    
    public void CreateMapReduceFiles(String demo, String times) throws UnknownHostException, IOException, FileNotFoundException, JSONException{
        
        // Path of Project data.
        String bigPath = "C:\\Users\\Administrator\\Documents\\NetBeansProjects\\MapReduce\\demo"+demo+"\\";
        
        // Path of input data.
        String path = bigPath + "application x" + times + "\\";
        
        String mpeg7SubPath = "mp7_out\\";
        String x3dSubPath = "x3d\\";
        
        String mpeg7FolderPath = path + mpeg7SubPath;
        String x3dFolderPath = path + x3dSubPath;
        
        File mpeg7Folder = new File(mpeg7FolderPath);
        File x3dFolder = new File(x3dFolderPath);
        
        String dbName = "demo" + demo + "x" + times;
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB db = mongoClient.getDB(dbName);
        
        CreateKeyValue jsonKeyValue = new CreateKeyValue(path, db);
        jsonKeyValue.CreateAllJSONFiles(mpeg7Folder, x3dFolder);
        
        mongoClient.close();
        
    }
}
