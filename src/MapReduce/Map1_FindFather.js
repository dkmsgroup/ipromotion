/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
// Purpose of map function is to find the root JSON documents.
// Only JSON documents that have children will have count = 1 and is_child = 0.
// Children objects, nodes that are not root and leafs, will have count = 0 and is_child = 1.

function map1_FindFather(){
    
    // whole name = "real_name - Copy ... ". For this application we want only "real_name".
    var whole_name = this.name;
    // Split the String at " " and create an array of subStrings.
    var name_of_strings = whole_name.split(" ");
    // Extract real_name.
    var real_name = name_of_strings[0];
    
    // "depth_x" is used to visit embedded JSON documents.
    // For JSON documents that are NOT leaf Objects.
    var depth_1 = this.Mpeg7.Description;
    if(depth_1){
        for(var i=0; i<depth_1.length; i++){
            var depth_2 = depth_1[i].MultimediaContent;
            if(depth_2){
                var depth_3 = depth_2.StructuredCollection;
                if(depth_3){
                    var depth_4 = depth_3.Collection;
                    if(depth_4){
                        for(var i=0; i<depth_4.length; i++){
                            var element = depth_4[i];
                            var element_id = element.id;
                            if(element_id == "Textures"){
                                var depth_5 = element.ContentCollection;
                                if(depth_5){
                                    // Create an array, where children of examined JSON document will be stored.
                                    var children = [];
                                    
                                    for(var j=0; j<depth_5.length; j++){
                                        // Take value of "depth_5[j].Content.Multimedia.MediaLocator.MediaUri" where the building (child) objects are stored.
                                        var values_string = depth_5[j].Content.Multimedia.MediaLocator.MediaUri;
                                        // Remove \" from the String.
                                        var newString = values_string.replace(/\"/g, "");
                                        // Split the String at " " and create an array of subStrings
                                        var array_of_strings = newString.split(" ");
                                        
                                        // Extract name of building (child) object.
                                        var only_name = array_of_strings[0].indexOf(".xml");
                                        var temp_name = array_of_strings[0].substring(0,only_name);
                                        
                                        // Extract size of building (child) object.
                                        var only_num = array_of_strings[1].indexOf("%");
                                        var temp_size = parseInt(array_of_strings[1].substring(0,only_num));
                                        temp_size /= 100;
                                        
                                        // Extract visible of building (child) object.
                                        only_num = array_of_strings[2].indexOf("%");
                                        var temp_visible = parseInt(array_of_strings[2].substring(0,only_num));
                                        temp_visible /= 100;
                                        
                                        // Calculate view of building (child) object with only two decimal places.
                                        var temp_view = Math.round((temp_size * temp_visible) * Math.pow(10,2))/Math.pow(10,2);
                                        
                                        // Create value of child object. To separate from root JSON document add field is_child = 1.
                                        var value = {
                                            cur_view : 0,
                                            count : 0,
                                            is_child : 1
                                        };
                                        // Emit child object.
                                        emit (temp_name, value);
                                        
                                        // Create cur_child. It contains the name of the child object and the temp_view.
                                        var cur_child = {
                                            name : temp_name,
                                            cur_view : temp_view
                                        };
                                        // Add cur_child to children array.
                                        children.push(cur_child);
                                    }
                                    // Create father object. It contains an array of children objects, is_child = 0, since it is a father, and count = 1.
                                    // count = 1 is added to determine how many root objects exist in collection.
                                    // In the reduce phase objects who are NOT root objects will be assigned with count = 0.
                                    var allChildren = {
                                        child : children,
                                        is_child : 0,
                                        count : 1
                                    };
                                    // Emit father object.
                                    emit(real_name, allChildren);
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    // For JSON documents that are leaf Objects.
    else {
        // Extract normalizedForm of examined JSON document.
        var only_num = this.Mpeg7.normalizedForm.indexOf("%");
        var temp_norm = parseInt(this.Mpeg7.normalizedForm.substring(0,only_num));
        
        // Extract r,g,b values of examined JSON document.
        var temp_r = parseInt(this.Mpeg7.color.red);
        var temp_g = parseInt(this.Mpeg7.color.green);
        var temp_b = parseInt(this.Mpeg7.color.blue);
        
        // Build value to be emitted.
        // Contains norma and r,g,b values. count = 0, since examined document is leaf.
        var value = {
            cur_view : 0,
            norma : temp_norm,
            count : 0,
            color : {
                r : temp_r,
                g : temp_g,
                b : temp_b
            }
        };
        emit(real_name, value);
    }
}