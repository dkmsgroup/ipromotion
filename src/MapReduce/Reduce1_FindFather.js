/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
// Purpose of reduce function is to find root JSON documents.
// Adds up all "is_child" values. If root object all "is_child" will be 0. If child element at least one "is_child" will be 1.
// For root objects sum_is_child = 0, for NOT root objects sum_is_child > 0.
// Adds up all "count" values to determine number of occurrence of examined object.
// Since we only want root objects to have count > 0, to start crossing the trees, if sum_is_child > 0, reduceVal.count = 0.

function reduce1_FindFather (keys,values){
    
    reduceVal = {
        cur_view : 0,
        count : 0,
        is_child : 0
    };
    
    for(var i=0; i<values.length; i++){
        // Sum "is_child" values.
        if(values[i].is_child){
            reduceVal.is_child += values[i].is_child;
        }
        
        // Sum "cur_view" values.
        if(values[i].cur_view){
            reduceVal.cur_view += values[i].cur_view;
        }
        
        // Sum "count" values.
        if(values[i].count){
            reduceVal.count += values[i].count;
        }
        
        // If objects has children, add child array.
        if(values[i].child){
            reduceVal.child = values[i].child;
        }
        
        // If object is a leaf add "norma" and "color" attributes.
        if(values[i].norma){
            reduceVal.norma = values[i].norma;
            
            reduceVal.color = {
                r : 0,
                g : 0,
                b : 0
            };
            
            reduceVal.color.r = values[i].color.r;
            reduceVal.color.g = values[i].color.g;
            reduceVal.color.b = values[i].color.b;
        }
    }
    
    // Update reduceVal.cur_view to have only two decimal places.
    reduceVal.cur_view = Math.round((reduceVal.cur_view) * Math.pow(10,2))/Math.pow(10,2);
    
    // If examined object is child, reduceVal.count = 0.
    if(reduceVal.is_child != 0){
        reduceVal.count = 0;
    }
    
    return reduceVal;
}