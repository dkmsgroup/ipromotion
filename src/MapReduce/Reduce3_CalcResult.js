/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
// Purpose of reduce function is to return all leaf objects.
// No sum is required. All objects are already met once.

function reduce3_CalcResult (keys,values){
    
    reduceVal = {
        whole : 0,
        color : {
            r : 0,
            g : 0,
            b : 0
        }
    };
    
    for(var i=0; i<values.length; i++){
        reduceVal.whole = values[i].whole;
        reduceVal.color.r = values[i].color.r;
        reduceVal.color.g = values[i].color.g;
        reduceVal.color.b = values[i].color.b;
    }
    
    return reduceVal;
}