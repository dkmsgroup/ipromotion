/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PathXML;

import common.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom2.JDOMException;

/**
 *
 * @author Administrator
 */
public class SearchPathXML {
    
    String elementName;
    String attributeName;
    String attributeValue;
    
    File resultsFile;
    DBCollection collection;
    DB db;
    
    
    private void SubmitQuery(String filePath) throws JDOMException, IOException{
        
        boolean matchQuery;
        File xmlFile = new File(filePath);
        
        NavigateXML navigate = new NavigateXML(elementName, attributeName, attributeValue);
        matchQuery = navigate.QueryXMLFile(xmlFile);
        
        /*
         * If query returns true write result to results file.
         */
        
        if(matchQuery){
            File curFile = new File(filePath);
            try (FileWriter fileWriter = new FileWriter(resultsFile, true)) {
                    fileWriter.write(new ProcessFilename().RemoveSuffix(curFile.getName()) + "\n");
                }
        }
    }
    
    /*
     * Get all documents of MongoDB collection "PathXML".
     * Get value of keys "Mpeg7" and "X3D" (the filepath of XML Description).
     * Submit Query to filepath.
     */
    
    public void Search() throws IOException, JDOMException{
        
        String filePath;
        
        DBCursor cursor = collection.find();
        
        while (cursor.hasNext()) {
            
            BasicDBObject nextDoc = (BasicDBObject) cursor.next();
            
            if(nextDoc.containsField("Mpeg7")){
                filePath = (String) nextDoc.get("Mpeg7");
                SubmitQuery(filePath);
            }
            
            if(nextDoc.containsField("X3D")){
                filePath = (String) nextDoc.get("X3D");
                SubmitQuery(filePath);
            }
            
	}
        
        //SubmitQuery(allFiles);
        
    }
    
    /*
     * Constructor
     */
    
    public SearchPathXML(String elName, String atName, String atValue, String path, String resultsFileName, DB database) throws IOException{
        elementName = elName;
        attributeName = atName;
        attributeValue = atValue;
        
        /*
         * Results written to path + "PathXML\\results\\" folder.
         */
        
        String resultsSubPath = "PathXML\\";
        File jsonSubFolder = new File(path + resultsSubPath);
        
        if(!jsonSubFolder.exists())
            jsonSubFolder.mkdir();
        
        resultsSubPath = "PathXML\\results\\";
        String resultsFolderPath = path + resultsSubPath;
        
        File resultsFolder = new File(resultsFolderPath);
        
        if(!resultsFolder.exists()){
            resultsFolder.mkdir();
        }
        
        /*
         * Compose name of results file.
         * Each query creates its own file.
         * If a file with the same name exists, overwrite it.
         */
        
        //String resultsFileName = elementName + " " + attributeName + " " + attributeValue + ".txt";
        String resultsFilePath = resultsFolderPath + resultsFileName;
        
        resultsFile = new File(resultsFilePath);
        
        if(!resultsFile.exists()){
            resultsFile.createNewFile();
        }
        else{
            try (FileWriter fileWriter = new FileWriter(resultsFile)) {
                fileWriter.flush();
            }
        }
        
        String colName = "PathXML";
        db = database;
        collection = db.getCollection(colName);
    
    }
}
