/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Legacy.*;
import PathXML.*;
import EmbeddedXML.*;
import KeyValue.*;
import common.*;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import org.jdom2.JDOMException;
import org.json.JSONException;

/**
 *
 * @author Administrator
 */
public class SearchCompare {

    
    public void SearchCompare(String times, int method, String elementName, String attributeName, Object attributeValue) throws UnknownHostException, IOException, JDOMException, JSONException{
        long startTime, endTime, duration;
        WriteResult writeResult = new WriteResult();
        
        // Path of Project data.
        String bigPath = "C:\\Users\\Administrator\\Documents\\NetBeansProjects\\";
        
        // Check if "Duration" folder exists.
        String durationSubPath = bigPath + "Duration\\";
        File tempFolder = new File(durationSubPath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Check if "Duration//Query" folder exists.
        String durationQueryPath = bigPath + "Duration\\Query\\";
        tempFolder = new File(durationQueryPath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Path to write the duration of the Query call.
        String durationQueryFilePath;
        // File to write the duration of the Query call.
        File durationFile;
        
        // Path to write the results (answers) of query.
        String path = bigPath + "application x" + times + "\\";
        
        String smallPath = elementName + " " + attributeName + " " + attributeValue;
        
        String mpeg7SubPath = "mp7_out\\";
        String x3dSubPath = "x3d\\";
        
        String mpeg7FolderPath = path + mpeg7SubPath;
        String x3dFolderPath = path + x3dSubPath;
        
        File mpeg7Folder = new File(mpeg7FolderPath);
        File x3dFolder = new File(x3dFolderPath);
        
        String dbName = "ipromx" + times;
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB db = mongoClient.getDB(dbName);
        
        switch(method){
            case 1:{
                startTime =  System.currentTimeMillis();
                SearchLegacy searchLegacy = new SearchLegacy(elementName, attributeName, attributeValue.toString(), path, smallPath);
                searchLegacy.Search(mpeg7Folder, x3dFolder);
                endTime =  System.currentTimeMillis();
                duration = endTime - startTime;
                
                durationQueryFilePath = durationQueryPath + smallPath + " SearchLegacy.txt";
                durationFile = new File(durationQueryFilePath);
                
                writeResult.WriteResult(duration, times, durationFile);
                //System.out.println("Done with SearchLegacy");
                
                break;
            }
            case 2:{
                startTime =  System.currentTimeMillis();
                SearchPathXML searchPathXML = new SearchPathXML(elementName, attributeName, attributeValue.toString(), path, smallPath, db);
                searchPathXML.Search();
                endTime =  System.currentTimeMillis();
                duration = endTime - startTime;
                
                durationQueryFilePath = durationQueryPath + smallPath + " PathXML.txt";
                durationFile = new File(durationQueryFilePath);
                
                writeResult.WriteResult(duration, times, durationFile);
                //System.out.println("Done with PathXML");
                
                break;
            }
            case 3:{
                startTime =  System.currentTimeMillis();
                SearchEmbeddedXML searchEmbeddedXML = new SearchEmbeddedXML(elementName, attributeName, attributeValue.toString(), path, smallPath, db);
                searchEmbeddedXML.Search();
                endTime =  System.currentTimeMillis();
                duration = endTime - startTime;
                
                durationQueryFilePath = durationQueryPath + smallPath + " EmbeddedXML.txt";
                durationFile = new File(durationQueryFilePath);
                
                writeResult.WriteResult(duration, times, durationFile);
                //System.out.println("Done with EmbeddedXML");
                
                break;
            }
            case 4:{
                startTime =  System.currentTimeMillis();
                SearchKeyValueQueue searchKeyValueQueue = new SearchKeyValueQueue(elementName, attributeName, attributeValue, path, smallPath, db);
                searchKeyValueQueue.Search();
                endTime =  System.currentTimeMillis();
                duration = endTime - startTime;
                
                durationQueryFilePath = durationQueryPath + smallPath + " KeyValueQueue.txt";
                durationFile = new File(durationQueryFilePath);
                
                writeResult.WriteResult(duration, times, durationFile);
                //System.out.println("Done with KeyValueQueue");
                
                break;
            }
        }
        
        mongoClient.close();
    }
}
