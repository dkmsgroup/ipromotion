/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import org.json.JSONException;

/**
 *
 * @author Administrator
 */
public class CreateFilesAll {
    
    /*
     * String times shows the number of copies of the original data.
     */
    
    private void CreateFilesWithMethod(int method) throws UnknownHostException, IOException, FileNotFoundException, JSONException{
        String times;
        
        for(int i=1; i<=32; i*=2){
            
            times = "" + i + "";
            System.out.println(times);
            
            CreateFiles createFiles = new CreateFiles();
            createFiles.CreateFiles(times, method);
        }
        
    }
    
    /*
     * Choose the method that the files are created.
     * method 1 : CreatePathXML (json docs with value the filepath of the mpeg7 and x3d description)
     * method 2 : CreateEmbeddedXML (json docs with value the mpeg7 and x3d description as an xml string)
     * method 3 : CreateKeyValue (json with the mpeg7 and x3d description as key/value pairs)
     * 
     * Set the dbName of MongoDB in which the created json files are stored.
     */
    
    public static void main(String[] args) throws UnknownHostException, IOException, FileNotFoundException, JSONException{
        
        CreateFilesAll create = new CreateFilesAll();
        
        for(int method=1; method<=3; method++){
            create.CreateFilesWithMethod(method);
            System.out.println("Done with method " + method);
        }
    }
}
