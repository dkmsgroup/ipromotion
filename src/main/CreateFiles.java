/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import PathXML.*;
import EmbeddedXML.*;
import KeyValue.*;
import common.*;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import org.json.JSONException;

/**
 *
 * @author Administrator
 */
public class CreateFiles {
    
    /*
     * Files are created using one of three methods.
     * method 1 : CreatePathXML (json docs with value the filepath of the mpeg7 and x3d description)
     * method 2 : CreateEmbeddedXML (json docs with value the mpeg7 and x3d description as an xml string)
     * method 3 : CreateKeyValue (json with the mpeg7 and x3d description as key/value pairs)
     * 
     * String times shows the number of copies of the original data.
     */
    
    public void CreateFiles(String times, int method) throws UnknownHostException, IOException, FileNotFoundException, JSONException{    
        long startTime, endTime, duration;
        WriteResult writeResult = new WriteResult();
        
        // Path of Project data.
        String bigPath = "C:\\Users\\Administrator\\Documents\\NetBeansProjects\\";
        
        // Check if "Duration" folder exists.
        String durationSubPath = bigPath + "Duration\\";
        File tempFolder = new File(durationSubPath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Check if "Duration//Create" folder exists.
        String durationCreatePath = bigPath + "Duration\\Create\\";
        tempFolder = new File(durationCreatePath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Path to write the duration of the Create call.
        String durationCreateFilePath;
        // File to write the duration of the Create call.
        File durationFile;
        
        // Path of input data.
        String path = bigPath + "application x" + times + "\\";
        
        String mpeg7SubPath = "mp7_out\\";
        String x3dSubPath = "x3d\\";
        
        String mpeg7FolderPath = path + mpeg7SubPath;
        String x3dFolderPath = path + x3dSubPath;
        
        File mpeg7Folder = new File(mpeg7FolderPath);
        File x3dFolder = new File(x3dFolderPath);
        
        String dbName = "ipromx" + times;
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB db = mongoClient.getDB(dbName);
        
        switch(method){
            case 1:{
                startTime =  System.currentTimeMillis();
                CreatePathXML jsonPathXML = new CreatePathXML(path, db);
                jsonPathXML.CreateAllJSONFiles(mpeg7Folder, x3dFolder);
                endTime =  System.currentTimeMillis();
                duration = endTime - startTime;
                
                durationCreateFilePath = durationCreatePath + "Create PathXML.txt";
                durationFile = new File(durationCreateFilePath);
                
                writeResult.WriteResult(duration, times, durationFile);
                //System.out.println("Done with CreatePathXML");
                
                break;
            }
            case 2:{
                startTime =  System.currentTimeMillis();
                CreateEmbeddedXML jsonEmbeddedXML = new CreateEmbeddedXML(path, db);
                jsonEmbeddedXML.CreateAllJSONFiles(mpeg7Folder, x3dFolder);
                endTime =  System.currentTimeMillis();
                duration = endTime - startTime;
                
                durationCreateFilePath = durationCreatePath + "Create EmbeddedXML.txt";
                durationFile = new File(durationCreateFilePath);
                
                writeResult.WriteResult(duration, times, durationFile);
                //System.out.println("Done with CreateEmbeddedXML");
                
                break;
            }
            case 3:{
                startTime =  System.currentTimeMillis();
                CreateKeyValue jsonKeyValue = new CreateKeyValue(path, db);
                jsonKeyValue.CreateAllJSONFiles(mpeg7Folder, x3dFolder);
                endTime =  System.currentTimeMillis();
                duration = endTime - startTime;
                
                durationCreateFilePath = durationCreatePath + "Create KeyValue.txt";
                durationFile = new File(durationCreateFilePath);
                
                writeResult.WriteResult(duration, times, durationFile);
                //System.out.println("Done with CreateKeyValue");
                
                break;
            }
        }
        
        mongoClient.close();
    }
}
