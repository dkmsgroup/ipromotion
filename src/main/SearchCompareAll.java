/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import org.jdom2.JDOMException;
import org.json.JSONException;

/**
 *
 * @author Administrator
 */
public class SearchCompareAll {
    
    /*
     * String times shows the number of copies of the original data.
     */
    
    private void SearchFilesWithMethod(int method, String elementName, String attributeName, Object attributeValue) throws UnknownHostException, IOException, FileNotFoundException, JSONException, JDOMException{
        String times;
        
        for(int i=1; i<=32; i*=2){
                times = "" + i + "";
                System.out.println(times);
                
                SearchCompare searchCompare = new SearchCompare();
                searchCompare.SearchCompare(times, method, elementName, attributeName, attributeValue);
        }
        
    }
    
    public static void main(String[] args) throws UnknownHostException, IOException, JDOMException, JSONException{
        
        SearchCompareAll search = new SearchCompareAll();
        String elementName = null, attributeName = null, smallPath, key = null;
        Object attributeValue = null, value = null;
        
        String times;
        
        for(int j=0; j<6; j++){
            
            switch(j){
                case 0: {
                    elementName = "Collection";
                    attributeName = "id";
                    attributeValue = "Transformations";
                    
                    key = "Mpeg7.Description.MultimediaContent.StructuredCollection.Collection.id";
                    value = "Transformations";
                    
                    break;
                }
                case 1: {
                    elementName = "Cylinder";
                    attributeName = "radius";
                    attributeValue = 0.5;
                    
                    key = "X3D.Scene.Group.Transform.Shape.Cylinder.radius";
                    value = 0.5;
                    
                    break;
                }
                case 2: {
                    elementName = "Geometry3D";
                    attributeName = "ObjectType";
                    attributeValue = "Box";
                    
                    key = "Mpeg7.Description.MultimediaContent.StructuredCollection.Collection."+
                            "DescriptorCollection.Descriptor.Geometry3D.ObjectType";
                    value = "Box";
                    
                    break;
                }
                case 3: {
                    elementName = "meta";
                    attributeName = "name";
                    attributeValue = "title";
                    
                    key = "X3D.head.meta.name";
                    value = "title";
                    
                    break;
                }
                case 4: {
                    elementName = "Transform";
                    attributeName = "DEF";
                    attributeValue = "TRANS";
                    
                    key = "X3D.Scene.Group.Transform.DEF";
                    value = "TRANS";
                    
                    break;
                }
                case 5: {
                    elementName = "X3D";
                    attributeName = "profile";
                    attributeValue = "Immersive";
                    
                    key = "X3D.profile";
                    value = "Immersive";
                    
                    break;
                }
            }
            
            smallPath = elementName + " " + attributeName + " " + attributeValue;
            System.out.println(smallPath);
            
            for(int method=1; method<=4; method++){
                search.SearchFilesWithMethod(method, elementName, attributeName, attributeValue);
                System.out.println("Done with method " + method);
            }
            
            for(int i=1; i<=32; i*=2){
                
                times = "" + i + "";
                System.out.println(times);
                
                SearchWithPath searchWithPath = new SearchWithPath();
                searchWithPath.SearchWithPath(times, smallPath, key, value);
            }
        }
    }
}
