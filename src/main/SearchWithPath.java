/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import KeyValue.*;
import common.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import org.json.JSONException;
/**
 *
 * @author Administrator
 */
public class SearchWithPath {
    public void SearchWithPath(String times, String smallPath, String key, Object value) throws UnknownHostException, IOException, JSONException{
        long startTime, endTime, duration;
        WriteResult writeResult = new WriteResult();
        
        // Path of Project data.
        String bigPath = "C:\\Users\\Administrator\\Documents\\NetBeansProjects\\";
        
        // Check if "Duration" folder exists.
        String durationSubPath = bigPath + "Duration\\";
        File tempFolder = new File(durationSubPath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Check if "Duration//Query" folder exists.
        String durationQueryPath = bigPath + "Duration\\Query\\";
        tempFolder = new File(durationQueryPath);
        
        if(!tempFolder.exists())
            tempFolder.mkdir();
        
        // Path to write the duration of the Query call.
        String durationQueryFilePath;
        // File to write the duration of the Query call.
        File durationFile;
        
        // Path to write the results (answers) of query.
        String path = bigPath + "application x" + times + "\\";
        
        String dbName = "ipromx" + times;
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        DB db = mongoClient.getDB(dbName);
        
        startTime =  System.currentTimeMillis();
        
        BasicDBObject query = new BasicDBObject();
        
        query.put(key,value);
        
        SearchKeyValuePath searchKeyValuePath = new SearchKeyValuePath(path, smallPath, db, query);
        searchKeyValuePath.Search();
        
        endTime =  System.currentTimeMillis();
        duration = (endTime - startTime);
        
        durationQueryFilePath = durationQueryPath + smallPath + " KeyValuePath.txt";
        durationFile = new File(durationQueryFilePath);
        
        writeResult.WriteResult(duration, times, durationFile);
        //System.out.println("Done with KeyValuePath");
        
        mongoClient.close();
    }
}
