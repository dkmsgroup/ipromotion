/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Administrator
 */
public class AddToMongoDB {
    
    File filesFolder;
    DBCollection collection;
    
    public void InsertFilesMongoDB() throws FileNotFoundException{
        DBObject dbObject;
        
        File[] jsonFiles = filesFolder.listFiles(new FilterJSONFiles());
        
        for(int i=0; i<jsonFiles.length; i++){
            
            File jsonFile = new File(jsonFiles[i].getPath());
            
            Scanner jsonContent = new Scanner(jsonFile);
            String jsonString = jsonContent.useDelimiter("\\Z").next();
            dbObject = (DBObject) JSON.parse(jsonString);
            collection.insert(dbObject);
        }
    }
    
    public void FolderToMongoDB(String path, DBCollection col) throws FileNotFoundException{
        
        filesFolder = new File(path);
        collection = col;
        
        InsertFilesMongoDB();
    }
}
