/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Administrator
 */
public class ModifyJSONFile {
    
    /*
     * Create JSONFile with the key/value pair specified.
     * Meant for the unique "name" key/value pair.
     */
    
    public void CreateJSONFile(String key, String value, String jsonFilePath) throws IOException, JSONException{
        
        JSONObject jObj = new JSONObject();
        
        jObj.put(key, value);
        
        try (FileWriter file = new FileWriter(jsonFilePath)) {
            file.write(jObj.toString());
            file.flush();
        }
        
    }
    
    /*
     * Add the key/value pair to the JSON file specified.
     */
    
    public void UpdateJSONFile(String key, Object value, String jsonFilePath) throws FileNotFoundException, IOException, JSONException{
        
        File jsonFile = new File(jsonFilePath);
        Scanner jsonContent = new Scanner(jsonFile);
        
        /*
         * Sets this scanner's delimiting pattern to the specified pattern, "\\Z".
         * "\\Z" matches the end of the input but for the final terminator, if any.
         * next() : Finds and returns the next complete token from this scanner.
         *          A complete token is preceded and followed by input that matches the delimiter pattern.
         * 
         * Pattern = end of input
         * next() returns the token from previous token (start of file), to next token (end of input).
         */
        
        String jsonString = jsonContent.useDelimiter("\\Z").next();
        
        JSONObject json = new JSONObject(jsonString);
        
        json.put(key,value);
                
        /*
         * Write the JSONObject to the file.
         */
        
        FileWriter fw = new FileWriter(jsonFilePath);
        fw.write(json.toString());
        fw.flush();
        fw.close();
        
    }
    
}
