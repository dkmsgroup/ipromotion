/*******************************************************************************
 * Copyright (C) 2014  Lydia Anyfantaki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Attribute;
import org.jdom2.JDOMException;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;
import org.jdom2.util.IteratorIterable;
import org.xml.sax.InputSource;
/**
 *
 * @author Administrator
 */
public class NavigateXML {
    
    String elementName;
    String attributeName;
    String attributeValue;
    
    /*
     * Navigate through the xml file specified in filePath.
     * Look for elements with name = "elementName", attribute = "attirbuteName" and attribute_value = "attributeValue".
     * If such element exists returns true, else false.
     */
    
    private boolean SubmitQueryInXMLFiles(Document doc) throws JDOMException, IOException{
        
        String curAttributeName;
        String curAttributeValue;
        
        /*
         * Get root element.
         */
        
        Element root = doc.getRootElement();
        
        /*
         * Check root node for match.
         */
        
        if(root.getName().equals(elementName))
            if (root.getAttributeValue(attributeName).equals(attributeValue))
                return true;
        
        /*
         * Get all Descendants of root (all elements) with element_name = elementName (through filter).
         */
        
        IteratorIterable<Element> allNodes = root.getDescendants(new ElementFilter(elementName));
        
        while(allNodes.hasNext()){
            
            Element curNode = allNodes.next();
            
            /*
             * Check if curNode has an attribute named "attributeName" with value "attributeValue".
             */
            /*
            if(curNode.hasAttributes()){
                if (curNode.getAttributeValue(attributeName) != null){
                    if(curNode.getAttributeValue(attributeName).equals(attributeName)){
                        return true;
                    }
                }
            }
            */
            /*
             * Get all attributes of filtered elements and check if 
             * same with given attributeName and attributeValue.
             */
            
            
            List<Attribute> allAttributes = curNode.getAttributes();
            
            for(int i=0; i<allAttributes.size(); i++){
                
                curAttributeName = allAttributes.get(i).getName();
                curAttributeValue = allAttributes.get(i).getValue();
                
                if((attributeName.equals(curAttributeName)) && (attributeValue.equals(curAttributeValue)))
                    return true;
            }
            
        }
        return false;
    }
    
    public boolean QueryXMLSrting(String xmlString) throws JDOMException, IOException{
        
        InputSource xmlFile = new InputSource(new StringReader(xmlString));
        
        /*
         * Builds a JDOM Document using a SAX Parser.
         */
        
        Document doc;
        SAXBuilder saxBuilder = new SAXBuilder();
        doc = saxBuilder.build(xmlFile);
        
        return SubmitQueryInXMLFiles(doc);
    }
    
    public boolean QueryXMLFile(File xmlFile) throws JDOMException, IOException{
        
        /*
         * Builds a JDOM Document using a SAX Parser.
         */
        
        Document doc;
        SAXBuilder saxBuilder = new SAXBuilder();
        doc = saxBuilder.build(xmlFile);
        
        return SubmitQueryInXMLFiles(doc);
    }
    
    public NavigateXML(String elName, String atName, String atValue){
        elementName = elName;
        attributeName = atName;
        attributeValue = atValue;
    }
    
}
